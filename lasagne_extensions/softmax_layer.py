# -*- coding: utf-8 -*-
"""
Created on Mon Aug 22 14:03:38 2016

@author: Peter
"""
import lasagne
import theano.tensor as T

#----------------------------------------------------------------------------------------------------

class Softmax4D(lasagne.layers.Layer):
    """
    Softmax layer.
    """

    def __init__(self, incoming, **kwargs):

        super(Softmax4D, self).__init__(incoming, **kwargs)

    def get_output_for(self, layer_input, **kwargs):

        self.num_units = 2

        # Reshape to (batch, n_classes, x+y).
        #
        reshaped_input_layer = layer_input.reshape((layer_input.shape[0], layer_input.shape[1], -1))

        # Reshape to (batch, n_classes, x+y)
        result_shape = (reshaped_input_layer.shape[0], 1, reshaped_input_layer.shape[2])
        layer_exp = T.exp(reshaped_input_layer - reshaped_input_layer.max(axis=1, keepdims=True).reshape(result_shape))

        # Reshape to (batch, n_classes, x+y) then to (batch, n_classes, x, y) -> (32, 2, 1, 1)
        #
        softmax_expression = (layer_exp / layer_exp.sum(axis=1, keepdims=True).reshape(result_shape)).reshape(layer_input.shape)

        return softmax_expression

    def get_output_shape_for(self, input_shape):

        # The output shape is the same as the input.
        #
        return input_shape
