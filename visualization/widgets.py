import numpy as np
from math import sqrt, ceil
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, RadioButtons
from mpl_toolkits.axes_grid1 import ImageGrid
from deeplearning import utils
from matplotlib.patches import Polygon
import matplotlib.colors as mcolors

traffic_light_LUT = {'red':   ((0.0, 0.0, 0.0),
                               (0.5, 1.0, 1.0),
                               (1.0, 1.0, 1.0)),
                     'blue':  ((0.0, 0.0, 0.0),
                               (0.5, 0.0, 0.0),
                               (1.0, 0.0, 0.0)),
                     'green': ((0.0, 1.0, 1.0),
                               (0.5, 1.0, 1.0),
                               (1.0, 0.0, 0.0)),
                     'alpha':  ((0.0, 1.0, 1.0),
                                (1.0, 1.0, 1.0)),}

traffic_light_cmap = mcolors.LinearSegmentedColormap('traffic_light',
                                                     traffic_light_LUT)

class SlideResultViewer(object):
    def __init__(self, original, level, result=None, annotations=None):
        self._original = original
        self._result = result
        self._annotations = annotations
        dims = self._original.getLevelDimensions(level)
        self._original_patch = self._original.getUCharPatch(0,0, dims[0],
                                                            dims[1], level)
        self._fig = plt.figure()
        plt.imshow(self._original_patch)
        self._downsample = self._original.getLevelDownsample(level)
        if annotations:
            for annotation in annotations:
                annotation.simplify(0,100)
                pts = [(p.getX() / self._downsample, p.getY() / self._downsample) for p in list(annotation.getCoordinates())]
                pol_ptch = Polygon(pts, edgecolor='black', facecolor='none', linewidth = 3.)
                plt.gca().add_patch(pol_ptch)
        if self._result:
            for current_level in xrange(self._result.getNumberOfLevels()):
                if dims == self._result.getLevelDimensions(current_level):
                    result_level = current_level
            self._result_patch = np.squeeze(self._result.getUCharPatch(0,0,dims[0],dims[1],result_level))
            plt.imshow(self._result_patch, cmap=traffic_light_cmap, alpha=0.2)
        plt.show()

class ScrollingImageViewer(object):
    def __init__(self, b, labels=None): 
        self._batch = b if len(b.shape) == 5 else b[None].transpose(1,0,2,3,4)
        self._labels = labels
        self._nr_scales = self._batch.shape[1]
        self._fig = plt.figure()
        self._axes = []
        self._imgs = []        
        for i in xrange(self._nr_scales):
            self._axes.append(self._fig.add_subplot(1, self._nr_scales, i+1))
            self._axes[i].autoscale(True)
            self._axes[i].set_xticklabels([])
            self._axes[i].set_yticklabels([])
            for tic in self._axes[i].xaxis.get_major_ticks():
                tic.tick1On = tic.tick2On = False
            for tic in self._axes[i].yaxis.get_major_ticks():
                tic.tick1On = tic.tick2On = False
            if self._labels.shape[1] == 2:
                color = "green"
                if self._labels[0,1] == 1:
                    color = "red"
                self._axes[i].spines['bottom'].set_color(color)
                self._axes[i].spines['top'].set_color(color)
                self._axes[i].spines['left'].set_color(color)
                self._axes[i].spines['right'].set_color(color)
        self._fig.subplots_adjust(left=0.25, bottom=0.25)
        self._fig.suptitle("Sample number 0")

        # plot first data set
        self._frame = 0
        for i in xrange(self._nr_scales):
            self._imgs.append(self._axes[i].imshow(self._batch[0,i]))

        # make the slider
        self._axframe = plt.axes([0.25, 0.1, 0.65, 0.03])
        self._sframe = Slider(self._axframe, 'Image', 0,
                              self._batch.shape[0] - 1, valinit=0, valfmt='%d')   
        # connect callback to slider   
        self._sframe.on_changed(self.update)
        plt.show()

    # call back function
    def update(self, val):
        frame = int(np.floor(self._sframe.val))
        for i in xrange(self._nr_scales):
            self._imgs[i].set_data(self._batch[frame, i])
            if self._labels.shape[1] == 2:
                color = "green"
                if self._labels[frame,1] == 1:
                    color = "red"
                self._axes[i].spines['bottom'].set_color(color)
                self._axes[i].spines['top'].set_color(color)
                self._axes[i].spines['left'].set_color(color)
                self._axes[i].spines['right'].set_color(color)            
            self._fig.suptitle("Sample number " + str(frame))
        plt.draw()
        
class ManualImageClassifier(object):
    def __init__(self, batch, labels, show_truth=False): 
        self._batch = batch if len(batch.shape) == 5 else batch[None].transpose(1,0,2,3,4)
        self._labels = labels
        self._nr_classes = labels.shape[1]
        self._nr_scales = self._batch.shape[1]
        self._fig = plt.figure()
        self._axes = []
        self._imgs = []
        self._manual_labels = []
        self._current_img = 0
        self._show_truth = show_truth
        for i in xrange(self._nr_scales):
            self._axes.append(self._fig.add_subplot(1, self._nr_scales, i+1))
            self._axes[i].autoscale(True)
            self._axes[i].set_xticklabels([])
            self._axes[i].set_yticklabels([])
            for tic in self._axes[i].xaxis.get_major_ticks():
                tic.tick1On = tic.tick2On = False
            for tic in self._axes[i].yaxis.get_major_ticks():
                tic.tick1On = tic.tick2On = False
        self._fig.subplots_adjust(left=0.25, bottom=0.25)
        self._fig.suptitle("Sample number " + str(self._current_img) + " out of " + str(self._batch.shape[0]))
        
        for i in xrange(self._nr_scales):
            self._imgs.append(self._axes[i].imshow(self._batch[self._current_img,i]))        
            if self._labels.shape[1] == 2 and self._show_truth:
                color = "green"
                if self._labels[self._current_img,1] == 1:
                    color = "red"
                self._axes[i].spines['bottom'].set_color(color)
                self._axes[i].spines['top'].set_color(color)
                self._axes[i].spines['left'].set_color(color)
                self._axes[i].spines['right'].set_color(color)               
        
        self._axframe = plt.axes([0.25, 0.1, 0.25, 0.25])
        self._label_buttons = RadioButtons(self._axframe,
                                           [cl_lab for cl_lab in xrange(self._nr_classes)])
        self._label_buttons.on_clicked(self._image_classified)
        plt.show()
    
    def _image_classified(self, label):
        if self._current_img >= self._batch.shape[0]:
            self._current_img = 0
            self.reset_accuracy()
        else:
            self._manual_labels.append(int(label))
            self._current_img += 1
        if self._current_img >= self._batch.shape[0]:
            self._fig.suptitle("Batch finished, accuracy was: " + str(self.get_current_accuracy()))                        
        else:
            for i in xrange(self._nr_scales):
                self._imgs[i].set_data(self._batch[self._current_img, i])
                if self._labels.shape[1] == 2 and self._show_truth:
                    color = "green"
                    if self._labels[self._current_img,1] == 1:
                        color = "red"
                    self._axes[i].spines['bottom'].set_color(color)
                    self._axes[i].spines['top'].set_color(color)
                    self._axes[i].spines['left'].set_color(color)
                    self._axes[i].spines['right'].set_color(color)
            self._fig.suptitle("Sample number " + str(self._current_img) + " out of " + str(self._batch.shape[0]))            
        plt.draw()
    
    def get_current_accuracy(self):
        nr_classified_samples = len(self._manual_labels)
        return np.sum(np.argmax(self._labels[:nr_classified_samples], axis=1) == np.array(self._manual_labels)) / float(nr_classified_samples)
        
    def reset_accuracy(self):
        self._manual_labels = []
        
        
class ScrollingFilterViewer(object):
    def __init__(self, filters, rgb=False, global_normalization=False):
        if isinstance(filters, np.ndarray):
            self._filters = [filters]
        else:
            self._filters = filters
        if global_normalization:
            for tp in self._filters:
                tp -= np.min(tp)
                tp /= np.max(tp)
        self._nr_filters = filters[0].shape[0]
        self._fig = plt.figure()
        self._rgb = rgb
        self._current_time_point = 0
        self._current_channel = 0
        self._axis = []
        self._img = []
        self._axis = self._fig.add_subplot(1, 1, 1)
        self._axis.autoscale(True)
        self._axis.set_xticklabels([])
        self._axis.set_yticklabels([])
        for tic in self._axis.xaxis.get_major_ticks():
            tic.tick1On = tic.tick2On = False
        for tic in self._axis.yaxis.get_major_ticks():
            tic.tick1On = tic.tick2On = False
        self._fig.subplots_adjust(left=0.25, bottom=0.25)

        # plot first data set
        self._frame = 0
        if self._rgb and self._filters[0].shape[1] == 3:
            self._plot_data = utils.filter_array_to_grid(self._filters[0])
        else:
            self._plot_data = utils.filter_array_to_grid(self._filters[0][:,0])
            # make the channel slider
            self._axframe = plt.axes([0.25, 0.1, 0.65, 0.03])
            self._sframe = Slider(self._axframe, 'Channel', 0,
                                  self._filters[0].shape[1] - 1, valinit=0, valfmt='%d')
            # connect callback to slider   
            self._sframe.on_changed(self.update_channel)
        if len(self._filters) > 1:
            self._axframe_tp = plt.axes([0.25, 0.15, 0.65, 0.03])
            self._sframe_tp = Slider(self._axframe_tp, 'Time Point', 0,
                                     len(filters) - 1, valinit=0, valfmt='%d')
            # connect callback to slider   
            self._sframe_tp.on_changed(self.update_timepoint)
        self._plot_data -= np.min(self._plot_data)
        self._plot_data /= np.max(self._plot_data)
        if self._rgb and self._filters[0].shape[1] == 3:        
            self._img = self._axis.imshow(self._plot_data)
        else:
            self._img = self._axis.imshow(self._plot_data, cmap='gray')
        plt.show()

    # call back function
    def update_channel(self, val):
        self._current_channel = int(np.floor(self._sframe.val))
        self._plot_data = utils.filter_array_to_grid(self._filters[self._current_time_point][:,self._current_channel])
        self._plot_data -= np.min(self._plot_data)
        self._plot_data /= np.max(self._plot_data)
        self._img.set_data(self._plot_data)
        plt.draw()
        
    # call back function
    def update_timepoint(self, val):
        self._current_time_point = int(np.floor(self._sframe_tp.val))
        cur_time_point_filters = self._filters[self._current_time_point]
        if self._rgb and self._filters[self._current_time_point].shape[1] == 3:            
            cur_time_point_filters -= np.min(cur_time_point_filters)
            cur_time_point_filters /= np.max(cur_time_point_filters)
            self._plot_data = utils.filter_array_to_grid(cur_time_point_filters)
        else:
            cur_time_point_filters[:,self._current_channel] -= np.min(cur_time_point_filters[:,self._current_channel])
            cur_time_point_filters[:,self._current_channel] /= np.max(cur_time_point_filters[:,self._current_channel])            
            self._plot_data = utils.filter_array_to_grid(cur_time_point_filters[:,self._current_channel])   
        self._img.set_data(self._plot_data)
        plt.draw()            
        

def visualize_weights(weights, global_normalization=False):
    """
    Plots convolutional filters in a grid
    
    First filters are normalized between 0 - 255 to allow visualization of 
    3-channel filters as RGB images and 1-channel images as gray scale.
    Global optimization normalizes all filters with respect to global 
    min-max-values.
    """
    nr_filters = weights.shape[0]
    per_row = int(0.5 + sqrt(nr_filters))
    nr_rows = int(ceil(nr_filters / float(per_row)))
    fig = plt.figure(1, (4., 4.))
    grid = ImageGrid(fig, 111,
                    nrows_ncols = (nr_rows, per_row),
                    axes_pad = 0.05,
                    )
    if global_normalization:
        utils.norm0255(weights)
    for i in range(nr_filters):
        grid[i].imshow(np.squeeze(utils.norm0255(weights[i])))
        grid[i].set_xticklabels([])
        grid[i].set_yticklabels([])
        for tic in grid[i].xaxis.get_major_ticks():
            tic.tick1On = tic.tick2On = False
        for tic in grid[i].yaxis.get_major_ticks():
            tic.tick1On = tic.tick2On = False
    plt.show()


def visualize_batch(b):
    """
    Plot a batch of image data
    
    Given a batch of data (given as a list of size 2, where index 0 is the
    batch itself and index 1 is the labels), plot the batch. In the case of
    two classes, the images are color-coded, green for class 0,
    red for class 1.
    
    Note: b[0] should have this dimensionality (image, channel, width, height),
    or, in the case of multi-scale batches, (image, scale, channel, width,
    height). b[1] should be one-hot encoded, so (image, classes).
    """
    if b[0].dtype == np.float32 and b[0].max() > 1.0:
        ptchs = utils.norm0255(b[0])
        labels = b[1]
    else :
        ptchs, labels = b
    if len(ptchs.shape) == 5:
        ptchs = ptchs.transpose(0,1,3,4,2)
    else: 
        ptchs = ptchs.transpose(0,2,3,1)
    viewer = ScrollingImageViewer(ptchs, labels)
    return viewer
    
def classify_batch(b, show_truth=False):
    """
    Plot a batch of image data
    
    Given a batch of data (given as a list of size 2, where index 0 is the
    batch itself and index 1 is the labels), plot the batch. In the case of
    two classes, the images are color-coded, green for class 0,
    red for class 1.
    
    Note: b[0] should have this dimensionality (image, channel, width, height),
    or, in the case of multi-scale batches, (image, scale, channel, width,
    height). b[1] should be one-hot encoded, so (image, classes).
    """
    if b[0].dtype == np.float32 and b[0].max() > 1.0:
        ptchs = utils.norm0255(b[0])
        labels = b[1]
    else :
        ptchs, labels = b
    if len(ptchs.shape) == 5:
        ptchs = ptchs.transpose(0,1,3,4,2)
    else: 
        ptchs = ptchs.transpose(0,2,3,1)
    viewer = ManualImageClassifier(ptchs, labels, show_truth)
    return viewer    