# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 12:37:57 2015

@author: Geert
"""

import os
import multiresolutionimageinterface as mir
import numpy as np
from scipy.ndimage import label
from scipy.ndimage.measurements import find_objects
from skimage.transform import resize
from batch_provider import BatchProvider


class WSIBatchProvider(BatchProvider):
    """
    Batch provider which uses whole slide images to provide batches
    
    The file_list should contain a list of whole slide images from which to
    extract patches. The other extra parameters are:
    - data_level: the resolution level from which to extract patches
    - msk_src: the source where the label masks for the whole slide images
                are stored. It can be either the path of a folder containing
                masks or a dictionary taht uses image file as key 
    - tis_fldr: the folder where the tissue masks for the whole slide images
                are stored
    - mask_level: the level of the mask images to use to determine to which
                  class a patch belongs
    - sample_class0_tissue_mask: when class 0 is not included in the label
                                 mask, should we sample it from the tissue
                                 mask?
    
    The WSIBatchProvider opens all whole slide images and keeps an open file
    handle to them for optimization purposes. Upon loading the mask images
    the bounding boxes for each labeled lesions are stored. Furthermore, the
    entire mask image is stored in memory at mask_level. This also is performed
    for the tissue mask, if provided. For each image it is tracked which
    classes it contains.
    
    During batch generation, depending on the setting of the balanced
    parameter, a file is selected for each sample of each class. Then a lesion
    bounding box from the corresponding class is selected randomly for each
    sample, weighted on the lesion size (larger lesions are more likely to be
    sampled, as they also cover more tissue). Within this bounding box a
    position is randomly sampled. At this position, the patch is extracted
    from the data image. If the image does not have a label mask, but does
    have a tissue mask and sample_class0_tissue_mask is set then class 0
    can also be sampled from these images.
    """
    def __init__(self, file_list, task_queue, result_queue,
                 nr_classes, batch_size, balanced=True, random_state=None,
                 patch_size=[128,128,3], data_level=0, msk_src="", masks_level=4,
                 sampling_src="", sampling_weight=1., sampler_level=4, 
                 sampler_level_difference = 0, sampler_classes=[0, 1]):
        super(WSIBatchProvider, self).__init__(file_list, task_queue,
                                               result_queue, nr_classes,
                                               batch_size, balanced,
                                               random_state)
        self._class_file_dict = {}
        self._bounding_box_indices = {}
        self._msk_src = msk_src
        self._sampling_src = sampling_src
        self._sampling_weight = sampling_weight
        self._sampler_level = sampler_level
        self._sampler_level_difference = sampler_level_difference
        self._sampler_classes = sampler_classes
        self._sampler_minmax = [0., 1.]
        self._sampler_max_tries = 20
        self._data_level = data_level
        self._masks_level = masks_level if masks_level >= 0 else data_level
        self._patch_size = patch_size
        
    def clean(self):
        super(WSIBatchProvider, self).clean()
        self._class_file_dict = {}
        self._bounding_box_indices = {}
        self._msk_src = ""
        self._tis_fldr = ""
        self._reader = None
        
    def initialize(self):
        super(WSIBatchProvider, self).initialize()
        self._logger.info("WSIBatchProvider.intialize function called")        
        for i in range(self._nr_classes):
            self._class_file_dict[i] = {}
            self._bounding_box_indices[i] = {}
        self._label_masks = {}
        self._sampler_masks = {}
        self._normals_from_abnormals = True
        self._reader = mir.MultiResolutionImageReader()
        if self._sampling_src:
            for index, fl in enumerate(self._file_list):
                if isinstance(self._sampling_src, basestring):
                  smp_file = os.path.join(self._sampling_src, os.path.splitext(os.path.basename(fl))[0] + ".tif")
                elif isinstance(self._msk_src, dict):
                  smp_file = self._sampling_src[fl]
                if os.path.isfile(smp_file):
                    self._logger.info("Sampling file " + smp_file + " is being processed.")
                    smp_wsi = self._reader.open(smp_file)
                    if smp_wsi:
                        if smp_wsi.getNumberOfLevels() < self._sampler_level:
                            self._logger.info("Sampler file " + smp_wsi + " does not have the required level, ignoring file.")
                            continue
                        dims = smp_wsi.getLevelDimensions(self._sampler_level)
                        if smp_wsi.getDataType() == mir.UChar:
                            smp_img = smp_wsi.getUCharPatch(0, 0, dims[0], dims[1], self._sampler_level)
                        else:
                            smp_img = smp_wsi.getFloatPatch(0, 0, dims[0], dims[1], self._sampler_level)
                        if self._nr_classes != 2 and smp_img.shape[2] <= max(self._sampler_classes):
                            self._logger.info("Sampler file " + smp_file + " does not have the required classes, ignoring file.")
                            smp_wsi.close()
                            continue
                        self._sampler_minmax = [smp_wsi.getMinValue(), smp_wsi.getMaxValue()]
                        self._sampler_masks[fl] = smp_img
                        smp_wsi.close()
        if self._msk_src:
            for index, fl in enumerate(self._file_list):
                if isinstance(self._msk_src, basestring):
                  msk_file = os.path.join(self._msk_src, os.path.splitext(os.path.basename(fl))[0] + ".tif")
                elif isinstance(self._msk_src, dict):
                  msk_file = self._msk_src[fl]
                if os.path.isfile(msk_file):
                    self._logger.info("Mask file " + msk_file + " is being processed.")
                    label_msk = self._reader.open(msk_file)
                    if label_msk:
                        if label_msk.getNumberOfLevels() < self._masks_level:
                            self._logger.info("Mask file " + msk_file + " does not have the required level, ignoring file.")
                            continue
                        dims = label_msk.getLevelDimensions(self._masks_level)
                        msk_img = label_msk.getUCharPatch(0, 0, dims[0], dims[1], self._masks_level)
                        for cur_label in range(self._nr_classes):
                            slices = find_objects(label(msk_img == cur_label + 1)[0])
                            bbox_ind = []
                            max_size = 0.0
                            for s in slices:
                                bbox_size = float((s[0].stop - s[0].start) * (s[1].stop - s[1].start))
                                if bbox_size > max_size:
                                    max_size = bbox_size
                                bbox_ind.append([s[0].start, s[0].stop,
                                                 s[1].start, s[1].stop, bbox_size])
                            # Make size relative and sort bbox_ind on size
                            for ind in range(len(bbox_ind)):
                                bbox_ind[ind][4] /= max_size
                            bbox_ind.sort(key=lambda x: x[4])
                            if slices:
                                data_img = self._reader.open(fl)
                                if data_img:
                                    self._logger.info("Mask file " + msk_file + " contains class " + str(cur_label))                                    
                                    if data_img.getNumberOfLevels() < self._data_level:
                                        self._logger.info("Data file " + fl + " does not have the required level, ignoring file.")
                                        continue
                                    self._class_file_dict[cur_label][fl] = data_img
                                    self._class_file_dict[cur_label][fl].setCacheSize(0)
                                    self._bounding_box_indices[cur_label][fl] = bbox_ind
                        self._label_masks[fl] = msk_img
                    else:
                        self.clean()
                        return -1
        self._logger.info("WSIBatchProvider.intialize function finished")
   
    def _handle_task(self):
        self._logger.info("WSIBatchProvider.__handle_task function called")
        data_downsample = 2**(self._data_level)
        msk_downsample_level0 = 2**(self._masks_level)
        msk_downsample_data_level = 2**(self._masks_level - self._data_level)
        smp_downsample_level0 = 2**(self._sampler_level + self._sampler_level_difference)
        ptch_size_msk = self._patch_size[0] / msk_downsample_data_level
        btch = np.zeros((self._batch_size, self._patch_size[2], self._patch_size[0], self._patch_size[1]), dtype='float32')
        labs = np.zeros((self._batch_size, self._nr_classes), dtype='float32')
        # added information about locations and source image (francesco)
        locs = [None]*self._batch_size
        imgs = [None]*self._batch_size
        loc_strings = []
        for cl_lab in self._class_file_dict.keys():
            self._logger.info("Current class:" + str(cl_lab))
            indices = self._random_state.randint(0, len(self._class_file_dict[cl_lab]),
                                        self._batch_size/len(self._class_file_dict.keys()))
            for i in range(self._batch_size/self._nr_classes):             
                fl = self._class_file_dict[cl_lab].keys()[indices[i]]
                self._logger.info("Current file:" + str(fl))
                img = self._class_file_dict[cl_lab][fl]
                rnd_ind = self._random_state.randint(self._bisect_bbox_size(self._random_state.rand(), self._bounding_box_indices[cl_lab][fl]), len(self._bounding_box_indices[cl_lab][fl]))
                bbox_ind = self._bounding_box_indices[cl_lab][fl][rnd_ind]
                self._logger.info("BBox indexes:" + str(bbox_ind))
                msk_img = self._label_masks[fl]
                smp_img = self._sampler_masks[fl] if self._sampler_masks.has_key(fl) else None
                loc = None
                cur_sample_sampler_weight = self._random_state.rand()
                sampler_tries = 0
                while loc is None:
                    if bbox_ind[1] - ptch_size_msk <= bbox_ind[0] or bbox_ind[3] - ptch_size_msk <= bbox_ind[2]:
                        loc = ((((bbox_ind[0] + bbox_ind[1]) / 2) + 1)*msk_downsample_level0, (((bbox_ind[2] + bbox_ind[3]) / 2) + 1)*msk_downsample_level0)
                        break
                    row = self._random_state.randint(bbox_ind[0], bbox_ind[1])
                    col = self._random_state.randint(bbox_ind[2], bbox_ind[3])
                    if msk_img[row, col] == (cl_lab + 1):
                        loc = (row*msk_downsample_level0, col*msk_downsample_level0)
                        if smp_img is not None and cl_lab in self._sampler_classes:                            
                            if cur_sample_sampler_weight < self._sampling_weight:                            
                                if smp_img.shape[2] == 1:
                                    lik = (float(smp_img[loc[0]//smp_downsample_level0, loc[1]//smp_downsample_level0, 0]) - self._sampler_minmax[0]) / (self._sampler_minmax[1] - self._sampler_minmax[0])
                                    if cl_lab == 1:
                                        lik = 1. - lik
                                else:
                                    lik = (float(smp_img[loc[0]//smp_downsample_level0, loc[1]//smp_downsample_level0, cl_lab]) - self._sampler_minmax[0]) / (self._sampler_minmax[1] - self._sampler_minmax[0])
                                    lik = 1. - lik
                                likelihood_threshold = self._random_state.rand()
                                self._logger.info("Evaluating sample difficulty at:" + str(loc))
                                self._logger.info("Sample difficulty score is:" + str(lik))
                                self._logger.info("and current likelihood threshold is: " + str(likelihood_threshold))
                                if lik < likelihood_threshold and sampler_tries < self._sampler_max_tries :
                                    self._logger.info("thus sample is rejected.")
                                    loc = None
                                    sampler_tries += 1
                                    continue
                            else:
                                self._logger.info("Sample difficulty not weighted: " + str(cur_sample_sampler_weight))
                self._logger.info("Selected sample location:" + str(loc))
                xpos = loc[1] - int(self._patch_size[0]/2) * data_downsample
                ypos = loc[0] - int(self._patch_size[1]/2) * data_downsample                
                if self._data_level < img.getNumberOfLevels():
                    ptch = img.getUCharPatch(xpos, ypos, self._patch_size[0], self._patch_size[1], self._data_level)
                else:
                    last_level = img.getNumberOfLevels() - 1
                    scale_difference = 2 ** (self._data_level - last_level)
                    ptch = img.getUCharPatch(xpos, ypos, self._patch_size[0] * scale_difference, self._patch_size[1] * scale_difference, last_level)
                    ptch = resize(ptch, (self._patch_size[0], self._patch_size[1]), preserve_range=True).astype("ubyte")
                self._logger.info("Sample sum:" + str(ptch.flatten().sum()))
                if self._debug:
                    loc_strings.append("Filename: " + fl + "\nLocation: " + str(loc) + "\nLevels: " + str(self._data_level) + "")
                btch_index = i + cl_lab*self._batch_size/len(self._class_file_dict.keys())
                btch[btch_index] = ptch.transpose(2, 0, 1)
                labs[btch_index] = self._nr_classes * [0]
                labs[btch_index][cl_lab] = 1
                # added by francesco                
                locs[btch_index] = loc
                imgs[btch_index] = fl
        perm = self._random_state.permutation(self._batch_size)
        btch = btch[perm]
        labs = labs[perm]
        locs = np.array(locs)[perm]
        imgs = np.array(imgs)[perm]
        if self._debug:
            loc_strings = list(np.array(loc_strings)[perm])
        self._logger.info("WSIBatchProvider.__handle_task function finished")
        if self._debug:
            return btch, labs, loc_strings
        else:
            return btch, labs, locs, imgs

    def _bisect_bbox_size(self, random_nr, bbox_list):
        lo = 0
        hi = len(bbox_list) - 1
        while lo < hi:
               mid = (lo+hi)//2
               if bbox_list[mid][4] < random_nr: lo = mid+1
               else: hi = mid
        return lo                    
        
class MultiScaleWSIBatchProvider(WSIBatchProvider):
    """
    Multi-scale version of the WSIBatchProvider
    
    Functionality of this class is similar to WSIBatchProvider (and can even be
    used in place off, but is a bit slower). The extra is that it also supports
    multi-scale batches by providing multi_scale_levels as a list of the levels
    used for multi-scale analysis.
    
    The output image data of the batch will have 5 dimensions: (batch_size, 
    nr_scales, nr_channels, width, height). The order of scales is similar to
    the order they are provided in in multi_scale_levels.
    The output label data will be the same as other BatchProviders (batch_size,
    nr_classes).
    """
    def __init__(self, file_list, task_queue, result_queue,
                 nr_classes, batch_size, balanced=True, random_state=None,
                 data_level=0, msk_src="", masks_level=4,
                 patch_size=[128,128,3], multi_scale_levels=[0], sampling_src="",
                 sampling_weight=1., sampler_level=4, sampler_level_difference = 0, sampler_classes=[0, 1]):
        super(MultiScaleWSIBatchProvider, self).__init__(file_list, task_queue,
                                                         result_queue,
                                                         nr_classes, batch_size, balanced, random_state, patch_size,
                                                         data_level, msk_src,
                                                         masks_level, sampling_src=sampling_src,
                                                         sampling_weight=sampling_weight, sampler_level=sampler_level,
                                                         sampler_level_difference=sampler_level_difference, sampler_classes=sampler_classes)
        self._multi_scale_levels = multi_scale_levels if multi_scale_levels else [self._data_level]
        
    def clean(self):
        super(WSIBatchProvider, self).clean()

    def _handle_task(self):
        self._logger.info("MultiScaleWSIBatchProvider.__handle_task function called")
        msk_downsample_level0 = 2**(self._masks_level)
        msk_downsample_data_level = 2**(self._masks_level - self._multi_scale_levels[0])
        smp_downsample_level0 = 2**(self._sampler_level + self._sampler_level_difference)        
        ptch_size_msk = self._patch_size[0] / msk_downsample_data_level
        btch = np.zeros((self._batch_size, len(self._multi_scale_levels),
                         self._patch_size[2], self._patch_size[0], self._patch_size[1]), dtype='float32')
        labs = np.zeros((self._batch_size, self._nr_classes), dtype='float32')
        # added information about locations and source image (francesco)
        locs = [None]*self._batch_size
        imgs = [None]*self._batch_size
        loc_strings = []
        for cl_lab in self._class_file_dict.keys():
            self._logger.info("Current class:" + str(cl_lab))
            indices = self._random_state.randint(0, len(self._class_file_dict[cl_lab]),
                                        self._batch_size/len(self._class_file_dict.keys()))
            for i in range(self._batch_size/len(self._class_file_dict.keys())):          
                fl = self._class_file_dict[cl_lab].keys()[indices[i]]
                self._logger.info("Current file:" + str(fl))
                img = self._class_file_dict[cl_lab][fl]
                rnd_ind = self._random_state.randint(self._bisect_bbox_size(self._random_state.rand(), self._bounding_box_indices[cl_lab][fl]), len(self._bounding_box_indices[cl_lab][fl]))
                bbox_ind = self._bounding_box_indices[cl_lab][fl][rnd_ind]
                self._logger.info("BBox indexes:" + str(bbox_ind))
                msk_img = self._label_masks[fl]
                smp_img = self._sampler_masks[fl] if self._sampler_masks.has_key(fl) else None
                loc = None
                cur_sample_sampler_weight = self._random_state.rand()
                sampler_tries = 0
                while loc is None:
                    if bbox_ind[1] - ptch_size_msk <= bbox_ind[0] or bbox_ind[3] - ptch_size_msk <= bbox_ind[2]:
                        loc = ((((bbox_ind[0] + bbox_ind[1]) / 2) + 1)*msk_downsample_level0, (((bbox_ind[2] + bbox_ind[3]) / 2) + 1)*msk_downsample_level0)
                        break
                    row = self._random_state.randint(bbox_ind[0], bbox_ind[1])
                    col = self._random_state.randint(bbox_ind[2], bbox_ind[3])
                    if msk_img[row, col] == (cl_lab + 1):
                        loc = (row*msk_downsample_level0, col*msk_downsample_level0)
                        if smp_img is not None and cl_lab in self._sampler_classes:                            
                            if cur_sample_sampler_weight < self._sampling_weight:                            
                                if smp_img.shape[2] == 1:
                                    lik = (float(smp_img[loc[0]//smp_downsample_level0, loc[1]//smp_downsample_level0, 0]) - self._sampler_minmax[0]) / (self._sampler_minmax[1] - self._sampler_minmax[0])
                                    if cl_lab == 1:
                                        lik = 1. - lik
                                else:
                                    lik = (float(smp_img[loc[0]//smp_downsample_level0, loc[1]//smp_downsample_level0, cl_lab]) - self._sampler_minmax[0]) / (self._sampler_minmax[1] - self._sampler_minmax[0])
                                    lik = 1. - lik
                                likelihood_threshold = self._random_state.rand()
                                self._logger.info("Evaluating sample difficulty at:" + str(loc))
                                self._logger.info("Sample difficulty score is:" + str(lik))
                                self._logger.info("and current likelihood threshold is: " + str(likelihood_threshold))
                                if lik < likelihood_threshold and sampler_tries < self._sampler_max_tries:
                                    self._logger.info("thus sample is rejected.")
                                    sampler_tries += 1
                                    loc = None
                                    continue
                            else:
                                self._logger.info("Sample difficulty not weighted: " + str(cur_sample_sampler_weight))                        
                self._logger.info("Sample location:" + str(loc))
                btch_index = i + cl_lab*self._batch_size/len(self._class_file_dict.keys())                
                for leven_nr, mlevel in enumerate(self._multi_scale_levels):
                    data_downsample = (2 ** mlevel)
                    xpos = loc[1] - int(self._patch_size[0]/2)*data_downsample
                    ypos = loc[0] - int(self._patch_size[1]/2)*data_downsample                    
                    if mlevel < img.getNumberOfLevels():
                        ptch = img.getUCharPatch(xpos, ypos, self._patch_size[0], self._patch_size[1], mlevel)                        
                    else:
                        last_level = img.getNumberOfLevels() - 1
                        scale_difference = 2 ** (mlevel - last_level)
                        ptch = img.getUCharPatch(xpos, ypos, self._patch_size[0] * scale_difference, self._patch_size[1] * scale_difference, last_level)
                        ptch = resize(ptch, (self._patch_size[0], self._patch_size[1]), preserve_range=True).astype("ubyte")
                    self._logger.info("Sample sum:" + str(ptch.flatten().sum()))
                    btch[btch_index][leven_nr] = ptch.transpose(2, 0, 1)
                if self._debug:
                    loc_strings.append("Filename: " + fl + "\nLocation: " + str(loc) + "\nLevels: " + str(self._data_level))                    
                labs[btch_index] = self._nr_classes * [0]
                labs[btch_index][cl_lab] = 1
                # added by francesco
                locs[btch_index] = loc
                imgs[btch_index] = fl
        perm = self._random_state.permutation(self._batch_size)
        btch = btch[perm]
        labs = labs[perm]
        locs = np.array(locs)[perm]
        imgs = np.array(imgs)[perm]
        if self._debug:
            loc_strings = list(np.array(loc_strings)[perm])
        btch = btch.squeeze()
        self._logger.info("MultiScaleWSIBatchProvider.__handle_task function finished")
        if self._debug:
            return btch, labs, loc_strings
        else:
            return btch, labs, locs, imgs

class DummyWSIBatchProvider(BatchProvider):
    def __init__(self, file_list, task_queue, result_queue,
                 nr_classes, batch_size, balanced=True, random_state=None, patch_size=[128,128,3],
                 data_level=0, msk_src="", masks_level=4,
                  sampling_src="",
                  sampling_weight=1., sampler_level=4, sampler_level_difference = 0, sampler_classes=[0, 1]):
        super(DummyWSIBatchProvider, self).__init__(file_list, task_queue,
                                               result_queue, nr_classes,
                                               batch_size, balanced, random_state)
        self._patch_size = patch_size
        
    def clean(self):
        super(DummyWSIBatchProvider, self).clean()
        
    def initialize(self):
        super(DummyWSIBatchProvider, self).initialize()
        
    def _handle_task(self):
        self._logger.info("DummyWSIBatchProvider.__handle_task function called")
        btch = self._random_state.rand(self._batch_size, self._patch_size[2], self._patch_size[0], self._patch_size[1]).astype('float32')
        labs = np.zeros((self._batch_size, self._nr_classes), dtype='float32')
        labs[range(self._batch_size), self._random_state.randint(0, self._nr_classes, self._batch_size)] = 1
        self._logger.info("DummyWSIBatchProvider.__handle_task function finished")
        return btch, labs
        
class DummyMultiScaleWSIBatchProvider(DummyWSIBatchProvider):
    def __init__(self, file_list, task_queue, result_queue,
                 nr_classes, batch_size, balanced=True, random_state=None, data_level=0, 
                 msk_src="", masks_level=4,
                 patch_size=[128,128,3], multi_scale_levels=[0], sampling_src="",
                 sampling_weight=1., sampler_level=4, sampler_level_difference = 0, sampler_classes=[0, 1]):
        super(DummyMultiScaleWSIBatchProvider, self).__init__(file_list, task_queue,
                                                         result_queue,
                                                         nr_classes, batch_size, balanced, random_state, patch_size,
                                                         data_level, msk_src,
                                                         masks_level, sampling_src=sampling_src,
                                                         sampling_weight=sampling_weight, sampler_level=sampler_level,
                                                         sampler_level_difference=sampler_level_difference, sampler_classes=sampler_classes)
        self._multi_scale_levels = multi_scale_levels if multi_scale_levels else [self._data_level]
        
    def clean(self):
        super(DummyMultiScaleWSIBatchProvider, self).clean()
        
    def initialize(self):
        super(DummyMultiScaleWSIBatchProvider, self).initialize()
        
    def _handle_task(self):
        self._logger.info("WSIBatchProvider.__handle_task function called")
        btch = self._random_state.rand(self._batch_size, len(self._multi_scale_levels), self._patch_size[2], self._patch_size[0], self._patch_size[1]).astype('float32')
        labs = np.zeros((self._batch_size, self._nr_classes), dtype='float32')
        labs[range(self._batch_size), self._random_state.randint(0, self._nr_classes, self._batch_size)] = 1
        return btch, labs   
    