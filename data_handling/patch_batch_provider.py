# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 12:37:57 2015

@author: Geert
"""

import os

from batch_provider import BatchProvider
from PIL import Image
import numpy as np

class PatchBatchProvider(BatchProvider):
    """
    Implementation of BatchProvider for a folder containing image files. Each
    image is considered a single sample. One requirement is that the class
    label is encoded in the filename as _classX_ is the class label, e.g.
    image1_class0_.png or image2_class1_x15_y14_.png.
    
    This class has one extra option compared to the base class, shuffled. This
    option determines whether the file list is traversed sequentially or in a
    random manner. 
    
    The batches are provided as list where index 0 contains the actual image
    data with dimensions (batch_size, nr_channels, width, height) and index 1
    contains the labels in one-hot format (batch_size, nr_classes).
    """    
    def __init__(self, file_list, task_queue, result_queue, nr_classes,
                 batch_size, balanced=True, random_state=None, 
                 patch_size=[3,128,128], shuffled=True):
        super(PatchBatchProvider, self).__init__(file_list, task_queue,
                                                 result_queue, nr_classes,
                                                 batch_size, balanced,
                                                 random_state)
        self._files_per_class = {}
        self._max_label = -1
        self._shuffled = shuffled
        self._cur_fl_index = 0
        self._cur_iteration = 0
        self._max_iterations = -1
        self._patch_size=patch_size
    
    def initialize(self):
        super(PatchBatchProvider, self).initialize()
        self._logger.info("PatchBatchProvider.intialize function called")
        for f in self._file_list:
            lab = int(f.split('_class')[1].split('_')[0])
            if lab > self._max_label:
                self._max_label = lab
            if not lab in self._files_per_class.keys():
                self._files_per_class[lab] = []
            self._files_per_class[lab].append(f)
        if self._nr_classes != len(set(self._files_per_class.keys())):
            self._logger.warning(("Provided number of classes is not the same as the amount of labels identified."))
        self._logger.info("PatchBatchProvider.intialize function finished")
        
    def clean(self):
        self._logger.info("PatchBatchProvider.clean function called")
        super(PatchBatchProvider, self).clean()
        self._files_per_class = {}
        self._max_label = -1
        self._shuffled = True
        self._cur_fl_index = 0
        self._max_iterations = -1       
        self._cur_iteration = 0
        self._logger.info("PatchBatchProvider.clean function finished")
        
    def start_new_epoch(self):
        self._cur_iteration = 0
        self._cur_fl_index = 0
        
    def set_max_iterations(self, max_iterations):
        self._max_iterations = max_iterations
        
    def get_max_iterations(self):
        return self._max_iterations
    
    def _handle_task(self):
        self._logger.info("PatchBatchProvider.__handle_task function called")
        self._cur_iteration += 1
        if (self._cur_iteration >= self._max_iterations
                and not self._max_iterations < 0):
            print "Max. nr. of iterations reached, start new epoch to continue"
            return None, None
        btch = np.zeros((self._batch_size, self._patch_size[0], self._patch_size[1], self._patch_size[2]), dtype='float32')
        labs = np.zeros((self._batch_size, self._max_label + 1),
                        dtype='float32')
        btch_fls = []
        if self._balanced:
            nr_fls_per_class = self._batch_size / self._nr_classes
            for cur_lab in self._files_per_class.keys():
                for j in range(nr_fls_per_class):
                    if self._shuffled:
                        index = self._random_state.randint(0,len(self._files_per_class[cur_lab]))
                        btch_fls.append(self._files_per_class[cur_lab][index])                            
                    else:                           
                        btch_fls.append(self._files_per_class[cur_lab][self._cur_fl_index])
                        self._cur_fl_index += 1
                        if self._cur_fl_index >= len(self._files_per_class[cur_lab]):
                            self._logger.info("Not enough files for new batch, starting new epoch")
                            self.start_new_epoch()
                            return None, None                            
        else:
            for i in range(self._batch_size):            
                if self._shuffled:
                    index = self._random_state.randint(0,len(self._file_list))
                else:
                    index = self._cur_fl_index
                    if self._cur_fl_index >= len(self._file_list):
                        self._logger.info("Not enough files for new batch, starting new epoch")
                        self.start_new_epoch()
                        return None, None                        
                f = self._file_list[index]
                btch_fls.append(f)
                self._cur_fl_index += 1
        self._random_state.shuffle(btch_fls)
        for i, b_f in enumerate(btch_fls):
            lab = int(b_f.split('_class')[1].split('_')[0])
            one_hot = (self._max_label + 1) * [0]
            one_hot[lab] = 1
            labs[i] = one_hot
            a = np.array(Image.open(b_f))
            if len(a.shape) == 2:
                a = a[None]
                btch[i] = a
            else:
                btch[i] = a.transpose(2,0,1)
        self._logger.info("PatchBatchProvider.__handle_task function finished")
        return btch, labs
     
class MultiScalePatchBatchProvider(PatchBatchProvider):
    """
    Extension of PatchBatchProvider for multi-scale analysis. This assumes that
    instead of a list of files a folder is provided. This folder should contain
    one folder per scale. By default all scales will be used, if a subset is 
    required, specify the folder names to use in the scales input variable.
    
    The returned image data in the batch will have 5 dimensions (batch_size,
    nr_scales, nr_channels, width, height). If scales is not specified, the
    order of the scales will be based on lexical sorting.
    """
    def __init__(self, file_list, task_queue, result_queue, nr_classes,
                 batch_size, balanced=True, random_state=None, 
                 patch_size=[3,128,128], shuffled=True, scales=[]):
        super(MultiScalePatchBatchProvider, self).__init__(file_list,
                                                           task_queue,
                                                           result_queue,
                                                           nr_classes, 
                                                           batch_size,
                                                           balanced=True,
                                                           patch_size=[3,128,128],
                                                           random_state=random_state,
                                                           shuffled=True)
        self._fldr_pth = file_list
        self._scales = scales
        self._file_list = None
    
    def initialize(self):
        self._logger.info("MultiScalePatchBatchProvider.intialize function called")
        if not self._scales:
            self._scales = sorted(os.listdir(self._fldr_pth))
        self._file_list = os.listdir(os.path.join(self._fldr_pth, str(self._scales[0])))
        super(MultiScalePatchBatchProvider, self).initialize()        
        self._logger.info("MultiScalePatchBatchProvider.intialize function finished")
        
    def clean(self):
        self._logger.info("MultiScalePatchBatchProvider.clean function called")
        super(MultiScalePatchBatchProvider, self).clean()
        self._file_list = None
        self._fldr_pth = ""
        self._scales = []
        self._logger.info("MultiScalePatchBatchProvider.clean function finished")
        
    def start_new_epoch(self):
        self._cur_iteration = 0
        self._cur_fl_index = 0
        
    def set_max_iterations(self, max_iterations):
        self._max_iterations = max_iterations
        
    def get_max_iterations(self):
        return self._max_iterations
    
    def _handle_task(self):
        self._logger.info("MultiScalePatchBatchProvider.__handle_task function called")
        self._cur_iteration += 1
        if self._cur_iteration >= self._max_iterations and not self._max_iterations < 0:
            print "Max. nr. of iterations reached, start new epoch to continue"
            return None, None
        btch = np.zeros((self._batch_size, len(self._scales), self._patch_size[0], self._patch_size[1], self._patch_size[2]), dtype='float32')
        labs = np.zeros((self._batch_size, self._max_label + 1), dtype='float32')
        btch_fls = []
        if self._balanced:
            nr_fls_per_class = self._batch_size / self._nr_classes
            for cur_lab in self._files_per_class.keys():
                for j in range(nr_fls_per_class):
                    if self._shuffled:
                        index = self._random_state.randint(0,len(self._files_per_class[cur_lab]))
                        btch_fls.append(self._files_per_class[cur_lab][index])                            
                    else:                           
                        btch_fls.append(self._files_per_class[cur_lab][self._cur_fl_index])
                        self._cur_fl_index += 1
                        if self._cur_fl_index >= len(self._files_per_class[cur_lab]):
                            print "Not enough files for new batch, starting new epoch"
                            self.start_new_epoch()
                            return None, None
        else:
            for i in range(self._batch_size):            
                if self._shuffled:
                    index = self._random_state.randint(0,len(self._file_list))
                else:
                    index = self._cur_fl_index
                    if self._cur_fl_index >= len(self._file_list):
                        print "Not enough files for new batch, starting a new epoch"
                        self.start_new_epoch()
                        return None, None                        
                f = self._file_list[index]
                btch_fls.append(f)
                self._cur_fl_index += 1
        self._random_state.shuffle(btch_fls)
        for i, b_f in enumerate(btch_fls):
            for j, s in enumerate(self._scales):
                lab = int(b_f.split('_class')[1].split('_')[0])
                one_hot = (self._max_label + 1) * [0]
                one_hot[lab] = 1
                labs[i] = one_hot
                a = np.array(Image.open(os.path.join(self._fldr_pth, s, b_f)))
                if len(a.shape) == 2:
                    a = a[None]
                    btch[i,j] = a
                else:
                    btch[i,j] = a.transpose(2,0,1)
        self._logger.info("MultiScalePatchBatchProvider.__handle_task function finished")
        return btch, labs
