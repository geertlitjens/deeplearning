# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 12:37:57 2015

@author: Geert
"""

import multiprocessing
import logging
import os
from numpy.random import RandomState

class BatchProvider(multiprocessing.Process):
    """
    Base class for BatchProvider's which run in a different process.
    
    BatchProvider takes a file list which it should be able to handle in the
    derived implementation. Furthermore, it gets a task_queue which contains
    one entry for each batch it is supposed to get. The BatchIterator will add
    items to this queue to request new batches. The batches are posted to the
    result_queue by this process. The number of classes needs to be specified,
    in addition to whether each batch should be balanced or not.
    
    The run function is performed until it
    returns. It quits when a None is encountered in the task_queue.
    The initialize function is called before the first batch is requested. As
    such all time-intensive preparation should go into this function.    
    """
    def __init__(self, file_list, task_queue, result_queue,
                 nr_classes, batch_size, balanced=True, random_state=None):
        multiprocessing.Process.__init__(self)
        self._balanced = balanced
        self._file_list = file_list
        self._task_queue = task_queue
        self._result_queue = result_queue
        self._nr_classes = nr_classes
        self._batch_size = batch_size
        self._logger = None          
        self._debug = False
        if random_state == None:
            self._random_state = RandomState(os.getpid())
        else:
            self._random_state = random_state
        
    def set_debug_mode(self, mode):
        self._debug = mode
        
    def debug_mode(self):
        return self._debug        
        
    def clean(self):
        self._balanced = True
        self._nr_classes = 0
        self._file_list = []
        self._batch_size = -1
        
    def initialize(self):
        if self._debug == False:
            self._logger = multiprocessing.log_to_stderr(level=logging.WARNING)
        else:
            self._logger = multiprocessing.log_to_stderr(level=logging.DEBUG)            
        self._logger.info("Initialized BatchProvider successfully")
        if self._balanced and self._batch_size % self._nr_classes != 0:
            self._logger.warning("Batch size " + str(self._batch_size) + " is not a multiple of the number of classes "  + str(self._nr_classes))
            self._logger.warning("Disabling balancing.")
            self._balanced = False
    
    def start_new_epoch(self):
        pass
    
    def run(self):
        self.initialize()
        self._logger.info("Run started successfully")        
        while True:
            tsk = self._task_queue.get()
            if tsk is None:
                self._result_queue.put(None)
                self._task_queue.task_done()
                break
            result = self._handle_task()
            self._result_queue.put(result)
            self._task_queue.task_done()
        self.clean()
        self._logger.info("Run finished successfully")
        return
        
    def _handle_task(self):
        return None, None
