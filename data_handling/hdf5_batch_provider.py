# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 12:37:57 2015

@author: Geert
"""

from batch_provider import BatchProvider
import tables
import numpy as np

class HDF5BatchProvider(BatchProvider):
    """
    """    
    def __init__(self, file_list, task_queue, result_queue, nr_classes,
                 batch_size, balanced=False, random_state=None, levels=[0]):
        super(HDF5BatchProvider, self).__init__(file_list, task_queue,
                                                result_queue, nr_classes,
                                                batch_size, balanced, random_state)
        self._dataset = tables.open_file(self._file_list[0], "r")
        self._sample_size = self._dataset.get_node('/level_0')[0]['inputs'].shape
        self._max_label = self._dataset.get_node('/level_0')[0]['targets'].shape[0] - 1
        self._total_samples = self._dataset.get_node('/level_0').shape[0]
        self._available_levels = [int(node.name.split("_")[1]) for node in self._dataset.list_nodes("/")]
        self._dataset.close()    
        self._levels = levels

    def get_number_of_samples(self):
        return self._total_samples
    
    def initialize(self):
        super(HDF5BatchProvider, self).initialize()
        self._logger.info("HDF5BatchProvider.intialize function called")
        self._dataset = tables.open_file(self._file_list[0], "r")
        self._level_nodes = {node_level : self._dataset.get_node('/level_' + str(node_level)) for node_level in self._available_levels}
        self._cur_index = 0
        self._logger.info("HDF5BatchProvider.intialize function finished")
        
    def clean(self):
        self._logger.info("HDF5BatchProvider.clean function called")
        super(HDF5BatchProvider, self).clean()
        self._max_label = -1
        self._total_samples = 0   
        self._cur_index = 0
        self._labels = None
        self._inputs = None
        self._dataset.close()
        self._logger.info("HDF5BatchProvider.clean function finished")
          
    def _handle_task(self):
        self._logger.info("HDF5BatchProvider.__handle_task function called")
        btch = np.empty((self._batch_size, len(self._levels), self._sample_size[0], self._sample_size[1], self._sample_size[2]), dtype="float32")
        labs = np.empty((self._batch_size, self._max_label + 1), dtype="float32")
        for i in xrange(self._batch_size):
            for level_i, level in enumerate(self._levels):
                level_node = self._level_nodes[level]            
                cur_row = level_node[self._cur_index]
                btch[i, level_i] = cur_row['inputs']
                labs[i] = cur_row['targets']
            self._cur_index += 1
            if self._cur_index >= self._total_samples:
                self._cur_index = 0
                self._logger.info("HDF5BatchProvider.__handle_task: reset index to zero")
        btch = np.squeeze(btch)
        self._logger.info("HDF5BatchProvider.__handle_task: Acquired batch size is " + str(btch.shape))
        self._logger.info("HDF5BatchProvider.__handle_task function finished")
        return btch, labs, None, None
