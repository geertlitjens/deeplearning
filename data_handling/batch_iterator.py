# -*- coding: utf-8 -*-
"""
Created on Thu Apr 23 14:05:37 2015

@author: Geert
"""

import multiprocessing

class BatchIterator(object):
    """
    Class which allows iteration over datasets in a separate process
    
    Using the getBatch gets a batch of data from the provider. file_list should
    contain a list of files which the provider should be able to handle, e.g.
    for the PatchBatchProvider it is a simple list of of image file paths.
    The provider argument should be the class name, so not a class instance,
    the BatchIterator will instantiate the class. You have to specify the 
    number of classes for the batch iterator. bpargs contains the other
    keyword arguments for the specific BatchProvider.
    
    After instantiation of the BatchIterator, call start to move the
    BatchProvider to a different process and start it. After experiments,
    do not forget to run stop to close down the other process.
    """
    def __init__(self, file_list, provider, nr_classes=2, batch_size=32,
                 nr_of_cached_samples=100, **bpargs):
        self._file_list = file_list
        self._nr_classes = nr_classes
        self._batch_size = batch_size
        self._tasks = multiprocessing.JoinableQueue()
        self._nr_of_cached_samples = nr_of_cached_samples
        for i in range(self._nr_of_cached_samples):
            self._tasks.put(1)
        self._results = multiprocessing.Queue()
        self._BP = None
        self._provider = provider
        self._bpargs = bpargs

    def __del__(self):
        self.stop()

    def start(self, debug=False):
        if self._BP is None:
            self._BP = self._provider(self._file_list, self._tasks,
                                      self._results, self._nr_classes,
                                      self._batch_size,
                                      **self._bpargs)
        if debug:
            self._BP.set_debug_mode(True)
        if self._tasks.empty():
            for i in range(self._nr_of_cached_samples):
                self._tasks.put(1)
        self._BP.start()

    def getBatch(self):
        if self._BP:
            self._tasks.put(1)
            return self._results.get()
    
    def nrOfTasks(self):
        print self._tasks.qsize()
        
    def terminate(self):
        if self._BP:
            self._BP.terminate()

    def stop(self):
        if self._BP and self._BP.is_alive():
            self._tasks.put(None) # Poison pill
            print "Joining tasks"
            self._tasks.join()
            print "Finished task, empyting results"
            while self._results.get() is not None:
                pass
            print "Joining process"
            self._BP.join()
