from deeplearning.data_handling.wsi_batch_provider import DummyMultiScaleWSIBatchProvider as MultiScaleWSIBatchProvider 
from deeplearning.data_handling.batch_iterator import BatchIterator

if __name__ == '__main__':                   
    fls = [
    r"D:\Code\sources\deeplearning\data_handling\test\data\T12-27902-6_APERIO.svs",
    r"D:\Code\sources\deeplearning\data_handling\test\data\T12-21259-6_01_APERIO.svs"
    ]
    
    msk_fldr = r"D:\Code\sources\deeplearning\data_handling\test\data\labelmask"
    smp_fldr = r"D:\Code\sources\deeplearning\data_handling\test\data\samplingmask"
    
    bi = BatchIterator(fls, batch_size=12, nr_of_cached_samples=24,
                       data_level=0, nr_classes=2, provider=MultiScaleWSIBatchProvider,
                       balanced=True, msk_src=msk_fldr, masks_level=6, 
                       multi_scale_levels=[2,3,4], patch_size=[256,256,3],
                       sampling_src=smp_fldr, sampling_weight=.8, sampler_level=4,
                       sampler_level_difference=3, sampler_classes=[0])
    bi.start(True)
        