import numpy as np
import logging
logging.basicConfig()
from deeplearning import utils
from skimage.transform import resize
import lasagne
import lasagne.layers as L
import collections
from math import ceil
import theano
import theano.tensor as T
import deeplearning.lasagne_extensions.softmax_layer as dlex_sml
from tqdm import trange
from PIL import Image

INTERP_ORDER_TO_NAME= {0:'edge', 1:'bilinear'}

class ImageClassifier(object):
    """
    Classify larger image using patch-trained convolution neural networks.
    
    The class provides two strategies to classify the images, either through
    patch-by-patch classification or by using a fully convolutional network.
    Furthermore, it provides functionality for tile-by-tile analysis of images
    which do not fit into GPU memory as a whole. It requires quite some
    variables to be set, which is handled through getters and setters. 
    
    It also support multi-scale classification, but this cannot be handled in a
    fully-convolutional manner. In the case of fully convolutional analysis the
    individual branches will be handle in a fully convolutional manner, after
    fusion the merged 'trunk' will be handled in a patch-base fashion. This is
    needed because when moving to bigger images than the trained patches, size
    differences occur in the feature maps of the indiviudal branches. Upscaling
    or downscaling of branches results in resolution differences, which cannot
    be handled in a fully convolutional fashion. As such these differences are
    handled patch-based.
    
    The rest of the functionality and implementation details are covered in the
    docstrings of the specific member functions.
    
    Note: this class does not transform a non-fully convolutional network to a
    fully convolutional network for you, you have to do this yourself, but it
    is pretty straightforward. It does adapt multi-scale fully-convolutional
    networks to work on larger images. 
    """
    def __init__(self):
        self._logger = logging.getLogger("deep_learning")        
        self._model = None
        self._mask = None
        self._mask_level = 0
        self._mode = "fully_convolutional"
        self._tile_size = 0
        self._postprocess_function = None
        self._preprocess_function = None
        self._model_patch_size = []
        self._scales = [1]
        self._output_scale = 0
        self._stride = -1
        self._soft = True
        self._force_processing = False
        self._input = None
        self._input_crops = None
        self._result = None
        self._output_class = -1
        self._lut = None
        self._quantize = False
        self._quantize_min = 0.
        self._quantize_max = 1.
        self._verbose = True
        self._rot_augment = False
        
    ###########################
    # Setter/getter functions #
    ###########################
    def set_input(self, images, mask=None, mask_level=0):
        """
        Provide the input image(s) for the ImageClassifier to process
        
        In the multi-resolution case, images can be provided as having the same
        size with the same center pixel or cropped to the appropriate size with
        the same center pixel. In the former case, the classifier will
        determine the correct areas to use.
        A mask image is optional, this masks out all zero values of the mask in
        the result image. Mask is expected to be of the same size as the image
        at the output_scale.
        """
        self._input = images
        self._mask = mask
        self._mask_level = mask_level
        if not isinstance(self._input, collections.Sequence):
            self._input = [self._input]
        self._input_crops = [[0, shp[-2], 0, shp[-1]] for shp in self._get_input_image_shapes()]
        
    def set_output_class(self, output_class):
      self._output_class = output_class
      
    def get_output_class(self):
      return self._output_class
        
    def get_result(self):
        """
        Get the result image
        """
        return self._result
    
    def set_model(self, model):
        """
        Setter for the Lasagne model
        """                        
        if isinstance(model, L.Layer):
            self._model = model
        else:
            self._logger.error("Invalid model given, should be a Lasagne \
                                model")
                                  
    def model(self):
        """
        Getter for the Lasagne model
        """                
        return self._model
        
    def set_mode(self, mode):
        """
        Provides the classification mode, can be fully_convolutional or
        patch_based
        """
        if mode == "fully_convolutional" or mode == "patch_based":
            self._mode = mode
        else:
            self._logger.error(("Invalid mode specified, valid options are: "
                                "fully_convolutional or patch_based"))
    
    def mode(self):
        """
        Getter for the classification mode
        """          
        return self._mode
    
    def set_tile_size(self, tile_size):
        """
        If analysis is to be performed per tile, provide a tile size
        """          
        if tile_size > -1 and tile_size % 1 == 0:
            self._tile_size = tile_size
        else:
            self._logger.error(("Invalid value for tile size, can only "
                                "be 0 or a positive integer"))
        
    def tile_size(self):
        """
        Getter for the tile_size
        """          
        return self._tile_size
    
    def set_model_patch_size(self, patch_size):
        """
        Provide the patch size with which the model was trained
        
        Patch size cannot always be inferred from the model but is important
        to prevent edge effects, especially in tiled analysis.
        """
        if len(patch_size) == 3:
            self._model_patch_size = patch_size        
        else:
            self._logger.error(("Patch size has to be a list of length 3 "
                                "containg channels, rows, columns"))
                                  
    def model_patch_size(self):
        """
        Getter for the model patch size
        """
        return self._model_patch_size
    
    def set_scales(self, scales):
        """
        Provide the scales of the input images.
        
        Scales needs to be the relative scales of the input images, e.g. the
        first input image is always scale 1, if the second image has a 4 times
        higher resolution, the scale would be 4.
        """
        self._scales = scales
        if self._input:
            self.set_input(self._input, self._mask, self._mask_level)
        
    def scales(self):
        """
        Getter for the the provided scales
        """
        return self._scales
        
    def set_lut(self, lut):
        """
        Set the look-up-table for stain normalization.
        """
        self._lut = lut
        
    def lut(self):
        """
        Getter for the look-up-table
        """
        return self._lut
    
    def set_preprocess_function(self, preprocess_function):
        """
        Setter for preprocessing function for tiles to be classified
        
        Sets a function to preprocess tiles to be classified.
        """
        self._preprocess_function = preprocess_function    
    
    def _preprocess_function(self):
        """
        Getter for preprocessing function for tiles to be classified
        
        Gets the current function used to preprocess tiles to be classified.
        """      
        return self._preprocess_function    
    
    def set_postprocess_function(self, postprocess_function):
        """
        Setter for postprocessing function for classified tiles
        
        Sets a function to postprocess classified tiles.
        """
        self._postprocess_function = postprocess_function
        
    def postprocess_function(self):
        """
        Getter for postprocessing function for classified tiles
        
        Gets the current function used to postprocess classified tiles.
        """      
        return self._postprocess_function
    
    def set_output_scale(self, output_scale):
        """
        Set the output scale for the result.
        
        The output scale is specified as an index into self_scales. E.g. if 
        self._scales = [1,4,5] and output_scale is specified as 1 the output
        scale of the result would be 4.
        """
        if output_scale < len(self._scales) and output_scale >= 0:
            self._output_scale = output_scale
        else:
            self._logger.error(("Output scales has to be one of the input "
                                "scales"))    
    
    def output_scale(self):
        """
        Getter for the output scale
        """
        return self._output_scale
        
    def set_stride(self, stride):
        """
        Set the stride for patch-based analysis
        
        The stride the step size between each patch is classified. The rest of
        the pixels are inferred through interpolation.
        """
        if stride > 0 and stride % 1 == 0:
            self._stride = stride
        else:
            self._logger.error(("Invalid value for stride, has to be an int "
                                "larger than 0"))
        
    def stride(self):
        """
        Getter for stride
        """
        return self._stride
        
    def soft(self):
        """
        Getter for soft
        """        
        return self._soft
        
    def set_soft(self, soft):
        """
        Sets whether soft classification should be performed
        
        Soft classification results in a probability per class, hard
        classification only an overall label.
        """        
        if soft == True or soft == False:
            self._soft = soft
        else:
            self._logger.error(("Invalid value for soft, can only be True "
                                "or False"))
        
    def force_processing(self):
        """
        Getter for force processing
        """
        return self._force_processing
        
    def set_force_processing(self, force_processing):
        """
        Set force processing for patch-based analysis.
        
        Patch analysis warns if it expects to use a large amount of system
        memory. If you want to ignore this warning, set force_processing to
        True
        """
        self._force_processing = force_processing

    def set_verbose(self, verbose):
        """
        Set verbosity. If False all prints to screen are suppressed.
        """
        self._verbose = verbose

    def set_rotational_augmentation(self, rot_augment):
        """
        Enable or disable test time rotational augmentation. If enabled the
        pathces are processed 4 times with 0, 90, 180, 270 degree of rotation
        and the results averaged.
        """
        self._rot_augment = rot_augment
        
    def _get_input_image_shapes(self):
        """
        Gets the input image shapes
        """
        return [img.shape for img in self._input]
        
    def _get_mask_shape(self):
        """
        Gets the shape of the mask
        """
        return self._mask.shape if self._mask is not None else []
        
    def get_input_image_crops(self):
        """
        Get input image crops
        """
        return self._input_crops
        
    def set_input_image_crops(self, crops):
        """
        Set input image crops
        
        Input image crops are used to only process part of the input image, or
        in multi-scale classification to determine the areas of the non-output
        scale images that should be used.
        """
        self._input_crops = crops
        
    def get_quantize(self):
        return self._quantize
        
    def set_quantize(self, quantize):
        self._quantize = quantize
        
    def set_quantize_min_and_max(self, qmin, qmax):
        self._quantize_max = qmax
        self._quantize_min = qmin
        
    def get_quantize_min_and_max(self):
        return (self._quantize_min, self._quantize_max)
        
    ########################################################
    # Functions which setup the actual processing pipeline #
    ########################################################
    def _check_parameters(self):
        """
        This function checks whether all the parameters are provided correctly        
        """
        if self._input is None:
            self._logger.error("No input images given, cannot calculate results.")
            return False
        if self._model is None:
            self._logger.error("No model given, cannot calculate results.")
            return False
        ls = L.get_all_layers(self._model)
        if not self._model_patch_size:
            if ls and ls[0].shape[1] and ls[0].shape[2] and ls[0].shape[3]:
                self._model_patch_size = list(ls[0].shape[1:])
        if self._get_input_image_shapes()[0][0] != self._model_patch_size[0]:
            self._logger.error(("Number of channels specified for a patch "
                                "is not equal to the number of channels of "
                                "the input image."))
            return False
        if (self._tile_size > 0 and
            (self._tile_size < self._model_patch_size[-1] or 
             self._tile_size < self._model_patch_size[-2])):
            self._logger.error(("Tile size for processing cannot be smaller "
                                "than patch size"))
            return False
        if (self._tile_size % float(self._model_patch_size[-1]) != 0 or 
            self._tile_size % float(self._model_patch_size[-2]) != 0):
            self._logger.warning(("It is recommended to select a tile_size "
                                  "which is equal to or a multiple of the "
                                  "patch_size and stride, otherwise stitching "
                                  "artifacts may occur."))
        if self._tile_size > 1024  and len(self._scales) > 1:
            self._logger.error(("Due to limitations in Theano's pooling "
                                "implementation we can currently not support "
                                "tiles larger than 1024 pixels in the "
                                "trailing dimensions"))
            return False
        if self._mode == "patch_based":
            if np.sum([self._stride % float(s) for s in self._scales]) > 0:
                self._logger.error(("Stride should be divisable by all "
                                    "scales, otherwise results cannot be "
                                    "calculated."))
                return False
            if self._tile_size > 0:
                out_image_size = (self._get_input_image_shapes()[self._output_scale][0], self._tile_size, self._tile_size)
            else:
                out_image_size = self._get_input_image_shapes()[self._output_scale]
            result_fill_area = (out_image_size[1] - self._model_patch_size[1],
                                out_image_size[2] - self._model_patch_size[2])
            patches_row = result_fill_area[0] // self._stride + 1
            patches_col = result_fill_area[1] // self._stride + 1
            total_nr_patches = ((patches_row * patches_col) // self._stride *
                                 len(self._scales))
            total_size = total_nr_patches * np.prod(self._model_patch_size) * 4
            if total_size > 1000000000 and not self._force_processing:
                self._logger.error(("Warning: you selected a stride such "
                                   "that more than 1GB of memory is needed "
                                   "(" + str(total_size) +"), please "
                                   "increase the stride or set "
                                   "force_processing to True, this may "
                                   "crash if you run out of memory"))
                return False
        if self._mode == "fully_convolutional":
            ls = L.get_all_layers(self._model)
            for l in ls:
                if not (isinstance(l, L.InputLayer) or
                        isinstance(l, L.Conv2DLayer) or
                        isinstance(l, L.Pool2DLayer) or
                        isinstance(l, L.ConcatLayer) or
                        isinstance(l, L.NonlinearityLayer) or
                        isinstance(l, L.DropoutLayer) or
                        isinstance(l, L.BatchNormLayer) or 
                        isinstance(l, dlex_sml.Softmax4D) or
                        isinstance(l, L.ElemwiseSumLayer) or 
                        isinstance(l, L.dnn.Conv2DDNNLayer)):
                            self._logger.error(("Mode is fully_convolutional "
                                                "but model contains "
                                                "unsupported layers: "
                                                "" + str(l)))
                            return False
        if self._mask is not None and (self._get_input_image_shapes()[self._output_scale][1:] != list(self._get_mask_shape())):
            self._logger.error(("Warning: mask should have the same size as the input image of output_scale"))
            return False
        return True

    def _get_multi_scale_fully_convolutional_model(self):
        """
        Adapts a multi-scale fully-convolutional model for bigger images
        
        This functions analyzes the individual branches for each scale and
        builds a new model, in the case of multi-scale, it will provide a
        model for each branch and for the final 'trunk'. It return the
        the model for the 'trunk', the models for the branches (if any)
        and the input parameters for all models in a dictionary.
        """
        branches = []
        branch = []
        final_section = None
        ls = L.get_all_layers(self._model)
        inp_layers = [l for l in ls
                      if isinstance(l, L.InputLayer)]
        Xs = {}
        # Iterate over the branches
        for l_nr, l in enumerate(ls):
            # If a new input or concatenation layer is found, a new branch
            # is started.
            if (isinstance(l, L.InputLayer) or
                isinstance(l, L.ConcatLayer)):
                if branch:
                    branches.append(branch)
                branch = []
            # Keep track of the input variables
            if isinstance(l, L.input.InputLayer):
                X = T.ftensor4(name="input" + str(len(branches)))
                new_inp_lay = L.InputLayer(shape=l.shape, input_var=X)
                Xs[X.name] = X
                branch.append(new_inp_lay)
            # If a convolutional layer or pooling layer, add it to the current
            # branch.
            elif isinstance(l, L.DropoutLayer):
                branch.append(L.DropoutLayer(branch[-1],
                                             p=l.p,
                                             rescale=l.rescale))
            elif isinstance(l, L.NonlinearityLayer):
                branch.append(L.NonlinearityLayer(branch[-1],
                                                  nonlinearity=l.nonlinearity))
            elif isinstance(l, L.BatchNormLayer):
                branch.append(L.BatchNormLayer(branch[-1],
                                               axes=l.axes,
                                               epsilon=l.epsilon,
                                               alpha=l.alpha,
                                               beta=l.beta,
                                               gamma=l.gamma,
                                               mean=l.mean,
                                               inv_std=l.inv_std))
            elif isinstance(l, L.Conv2DLayer):
                bias = l.b.get_value() if l.b is not None else None
                branch.append(L.Conv2DLayer(branch[-1],
                                            num_filters=l.num_filters,
                                            pad=l.pad,
                                            filter_size=l.filter_size,
                                            nonlinearity=l.nonlinearity,
                                            W=l.W.get_value(),
                                            b=bias))
            elif isinstance(l, L.Pool2DLayer):
                branch.append(L.Pool2DLayer(branch[-1],
                                               pool_size=l.pool_size,
                                               stride=l.stride,
                                               pad=l.pad,
                                               ignore_border=l.ignore_border))
            elif isinstance(l, L.ConcatLayer):
                first_conv_layer_trunk_nr = l_nr + 1
                branch.append(l)
        # The last branch is essentially the 'stem' of the tree and leads to
        # the output.
        mgl = ls[first_conv_layer_trunk_nr]
        prev_weight_index = 0
        final_section = branch[2:]
        for branch_i, branch in enumerate(branches):
            scale_diff = (self._scales[self._output_scale] / self._scales[branch_i])
            num_filts_end = L.get_output_shape(branch[-1])[1]
            weights = mgl.W.get_value()[:, prev_weight_index:prev_weight_index + num_filts_end]
            bias = mgl.b.get_value() if mgl.b is not None else None
            if branch_i <= self._output_scale:
                new_conv_layer = L.Conv2DLayer(branch[-1], mgl.num_filters, mgl.filter_size, (int(scale_diff), int(scale_diff)),
                                               W=weights, b=bias, nonlinearity=mgl.nonlinearity)
                branches[branch_i].append(new_conv_layer)
            else:
                upsample = int(1./scale_diff)
                l_trans = L.Upscale2DLayer(branch[-1], upsample)
                l_trans_slice1 = L.SliceLayer(l_trans, indices=slice(None, -(upsample - 1)), axis=-1)
                l_trans_slice2 = L.SliceLayer(l_trans_slice1, indices=slice(None, -(upsample - 1)), axis=-2)
                new_conv_layer = L.DilatedConv2DLayer(l_trans_slice2, mgl.num_filters, mgl.filter_size, (upsample,upsample), W=weights, b=bias, nonlinearity=mgl.nonlinearity)
                branches[branch_i].extend([l_trans, l_trans_slice1, l_trans_slice2, new_conv_layer])
            prev_weight_index += num_filts_end
        sum_layer = L.ElemwiseSumLayer([branch[-1] for branch in branches])
        final_section[0].input_layer = sum_layer
        final_section.insert(0,sum_layer)
        # If there are no branches, the model is not multi-scale, but it should
        # still work.
        if len(branches) != len(inp_layers) and not len(self._scales) == 1:
            self._logger.error(("Found a different number of branches and "
                                "input layers, cannot execute computation"))
            return None, None
        return final_section[-1], branches, Xs
        
    def _get_prediction_function(self):
        """
        Get the compiled theano functions needed for image classification.
        
        Depending on the nature of the data (multi-scale or not) and the
        specified mode (patch-based or fully-convolutional) the required
        theano functions are generated. In the case of hard classification an
        extra argmax is added to the model to obtain a single output channel.
        """
        local_model = self._model
        pred_funs = []
        if len(self._scales) > 1 and self._mode == "fully_convolutional":
            local_model, branches, Xs = self._get_multi_scale_fully_convolutional_model()
        ls = L.get_all_layers(local_model)
        X = [l.input_var for l in ls
             if isinstance(l, L.InputLayer)]
        outp = L.get_output(local_model, deterministic=True)
        if self._soft:      
            pred_funs.insert(0, theano.function(X, outp))
        else:
            pred_funs.insert(0, theano.function(X, T.argmax(outp, axis=1)))
        return pred_funs, local_model
    
    def _preprocess_tile(self, tile, lut):
      if self._preprocess_function:
        return self._preprocess_function(tile, lut)
      else:
        return tile
        
    #####################################################
    # Functions to get data from the input images/masks #
    #####################################################
    def _get_image_data_from_input(self, box=[], scale=1):
        """
        Get pixel data from the input arrays, corrected for the input crops. 
        
        This function interprets box as coordinates starting at the crops. E.g.
        if the start_x in box = 0 and the crop of the input image is 15, 0 will
        be interpreted as 15.
        
        Preprocessing (if any) is applied.
        """
        img_nr = self._scales.index(scale)
        crop = self._input_crops[img_nr]
        if box:
            box[0] = box[0] + crop[0]
            box[1] = box[1] + crop[0] if box[1] + crop[0] <= crop[1] else crop[1]
            box[2] = box[2] + crop[2]
            box[3] = box[3] + crop[2] if box[3] + crop[2] <= crop[3] else crop[3]
            return self._preprocess_tile(self._input[img_nr][:, box[0]:box[1], box[2]:box[3]], self._lut)
        else:
            return self._preprocess_tile(self._input[img_nr][:, crop[0]:crop[1],crop[2]:crop[3]], self._lut)
            
    def _get_image_data_from_mask(self, box=[]):
        """
        Get pixel data from the mask array, corrected for the input crops. 
        
        See _get_image_data_from_input for more information.
        """
        crop = self._input_crops[self._output_scale]
        if box:
            box[0] = box[0] + crop[0]
            box[1] = box[1] + crop[0] if box[1] + crop[0] <= crop[1] else crop[1]
            box[2] = box[2] + crop[2]
            box[3] = box[3] + crop[2] if box[3] + crop[2] <= crop[3] else crop[3]
            return self._mask[box[0]:box[1], box[2]:box[3]]
        else:            
            return self._mask[crop[0]:crop[1],crop[2]:crop[3]]

    #######################################
    # Functions handling the result image #
    #######################################
    def _initialize_result(self):
        nr_channels = 1
        if self._soft and self._output_class < 0:
            nr_channels = self._model.output_shape[1]
        shp = self.get_input_image_crops()[self._output_scale]
        self._result = np.squeeze(np.zeros((nr_channels, shp[1]-shp[0], shp[3]-shp[2]), dtype="float32"))
        return
    
    def _get_result_shape(self):
        if not self._result is None:
            return self._result.shape        
        
    def _finish(self):
        pass
    
    #############################################
    # Functions for one-go-processing of images #
    #############################################
    def process(self):
        """
        Run the processing given inputs and parameters
        """
        self._result = None
        if not isinstance(self._input, collections.Sequence):
            self._input = [self._input]
        if self._check_parameters():
            input_shapes = self._get_input_image_shapes()
            offsets = self._calculate_image_offsets([None] + list(input_shapes[self._output_scale]), self._model)        
            # If the input_shapes are different assume we do not need to adapt, the user already did.
            if len(input_shapes) > 1:
                if input_shapes[0] == input_shapes[1]:
                    center = [input_shapes[0][-2] // 2, input_shapes[0][-1] // 2]
                    shapes = [[-(shp[-2] // (2*(scale / self._scales[0]))),
                               ceil(shp[-2] / float(2*scale / self._scales[0])),
                               -(shp[-1] // (2*scale / self._scales[0])),
                               ceil(shp[-1] / float(2*scale / self._scales[0]))]
                              for shp, scale in zip(input_shapes, self._scales)]
                    crops = [[center[0]+shp[0]+off[0], center[0]+shp[1]+off[1], center[1]+shp[2]+off[2], center[1]+shp[3]+off[3]] for shp, off in zip(shapes, offsets)]
                    self.set_input_image_crops(crops)            
            pred_funs, local_model = self._get_prediction_function()
            if self._tile_size == 0 and self._mode == "patch_based":
                self._result =  self._process_images_patch_based(pred_funs[0])
                if self._output_class >= 0:
                  self._result = self._result[self._output_class]                
            elif self._tile_size == 0 and self._mode == "fully_convolutional":
                self._result = self._process_images_fully_convolutional(pred_funs)
                if self._output_class >= 0:
                  self._result = self._result[self._output_class]
            elif self._tile_size > 0:
                self._process_images_tiled(pred_funs, local_model)
        else:
            self._logger.warning("One or more parameter values were invalid")
            return None

    def _feat_maps_to_tiles(self, feat_maps, fs):
        """
        Converts the final feature maps for individual scales to tiles.
        
        This functions essentially contains all the extra computations needed
        to allow multi-scale fully convolutional processing of an image. It
        converts the feature maps into the correct tiles for patch-based
        processing. Sadly, processing time of this function is quite
        substantial. Moving as many steps as possible to the GPU would
        significantely optimize execution time.
        """
        fw, fh = fs
        osc = float(self._scales[self._output_scale])        
        resize_factors = [(s/osc) if s/osc >= 1 else 1. for s in self._scales]
        feat_maps = [np.array([resize(feat_map[c], (rf*feat_map.shape[-2], rf*feat_map.shape[-1]), 1, 'edge', preserve_range=True) for c in range(feat_map.shape[0])], dtype="float32") for feat_map, rf in zip(feat_maps, resize_factors)]
        feat_dim = np.sum([fm.shape[0] for fm in feat_maps])
        nr_ptchs = (feat_maps[self._output_scale].shape[-2] - fw + 1) * (feat_maps[self._output_scale].shape[-1] - fh + 1)            
        in_ptchs = np.zeros((nr_ptchs, feat_dim, fw, fh), dtype="float32")
        cntr = 0
        exts  = [(fw*rf, fh*rf) if s > osc else (fw, fh) for s, rf in zip(self._scales, resize_factors)]
        strds = [(rf, rf) if s > osc else (1,1) for s, rf in zip(self._scales, resize_factors)]
        for i in range(feat_maps[self._output_scale].shape[-2] - fw + 1):
            for j in range(feat_maps[self._output_scale].shape[-1] - fh + 1):
                pos_i = [i if s > osc else i*(float(osc)/s) for s, rf in zip(self._scales, resize_factors)]
                pos_j = [j if s > osc else j*(float(osc)/s) for s, rf in zip(self._scales, resize_factors)]
                for c, (pos_s_i, pos_s_j, feat_mp, s, ex, st) in enumerate(zip(pos_i, pos_j, feat_maps, self._scales, exts, strds)):
                    in_ptchs[cntr,c*feat_mp.shape[0]:(c+1)*feat_mp.shape[0]] = feat_mp[:,pos_s_i:pos_s_i+ex[0]:st[0], pos_s_j:pos_s_j+ex[1]:st[1]]
                cntr += 1
        return in_ptchs

    def _rot90(self, image, k):
        """
        Rotate multi-channel image where the channel is the first dimension.
        """
        return np.rot90(image.transpose(1, 2, 0), k).transpose(2, 0, 1)

    def _process_images_fully_convolutional(self, pred_funs, images=None, msk_data=None):
        """
        Performs classification of an image in one go.
        
        This function using the prediction functions to classify the image
        given through images. If images contains multiple images, it is
        assumed that each index corresponds to the image of the corresponding
        scale in self._scales. If not images are given to this function, it
        will fill this variable from self._inputs. Optionally, msk_data can be
        provided to mask out certain parts of the image.
        """
        # Get image and mask data from self._input if it is not provided by
        #  the caller of this function.
        if images is None:
            images = [self._get_image_data_from_input(scale=s) for s in self._scales]
#        if self._mask is not None:
#            if msk_data is None:
#                msk_data = self._get_image_data_from_mask()            
        
        # Determine the amount of padding required for each side of the image.
        # Even filters lose more pixels at the right edge, depending on the 
        # padding settings, as do pooling layers. This needs to be corrected.
        ls = lasagne.layers.get_all_layers(self._model)
        if np.any([isinstance(layer, lasagne.layers.merge.ConcatLayer) for layer in ls]):
            last_branch_layer = ls[-2]
            first_fused_conv_layer = ls[-1]
            scale_count = 0
            found_layer = False
            for i, l in enumerate(ls[1:]):
                if isinstance(l, lasagne.layers.input.InputLayer) or isinstance(l, lasagne.layers.merge.ConcatLayer):
                    scale_count += 1
                    if scale_count > self._output_scale and not found_layer:
                        last_branch_layer = ls[i]
                        found_layer = True
                if isinstance(l, lasagne.layers.ConcatLayer):
                    first_fused_conv_layer = ls[i+2]
            pads, downsamples = utils.get_reconstruction_information(last_branch_layer, [None] + list(images[self._output_scale].shape))
            pads += np.array((((first_fused_conv_layer.filter_size[0] - 1) // 2) * downsamples[0],
                              int(ceil((first_fused_conv_layer.filter_size[0] - 1) / 2.) * downsamples[0]),
                              ((first_fused_conv_layer.filter_size[1] -1) // 2) * downsamples[1],
                              int(ceil((first_fused_conv_layer.filter_size[1] - 1) / 2.) * downsamples[1])))
        else:
            pads, downsamples = utils.get_reconstruction_information(ls[-1], [None] + list(images[self._output_scale].shape))
        
        # Calculate the actual classification result. In the case of
        # multi-scale processing we first obtain the feature maps directly
        # before concatenation layer. Subsequently, patch-based processing of
        # the feature maps is performed with the 'trunk' part of the model.
        # In the case of single-scale, the model is straightforwardly applied.
#==============================================================================
#         if len(self._scales) > 1:
#             fw, fh = first_fused_conv_layer.filter_size
#             feat_maps = [pred_fun(img[None])[0] for pred_fun, img in zip(pred_funs[1:], images)]
#             in_ptchs = self._feat_maps_to_tiles(feat_maps, (fw,fh))
#             classified_tiles = np.squeeze(pred_funs[0](in_ptchs)).transpose()
#             result = np.squeeze(classified_tiles.reshape(-1, feat_maps[self._output_scale].shape[-2] - fw + 1, feat_maps[self._output_scale].shape[-1] - fh + 1))
#         else:
#==============================================================================
        result = pred_funs[0](*[img[None] for img in images])[0]

        # Apply the test time rotational augmentation if configured.
        if self._rot_augment:
            result_rot_1 = self._rot90(pred_funs[0](*[self._rot90(img, 1)[None] for img in images])[0], -1)
            result_rot_2 = self._rot90(pred_funs[0](*[self._rot90(img, 2)[None] for img in images])[0], -2)
            result_rot_3 = self._rot90(pred_funs[0](*[self._rot90(img, 3)[None] for img in images])[0], -3)
            result = (result + result_rot_1 + result_rot_2 + result_rot_3) / 4.0

        if not self._soft:
            result = result[None]
        
        # Zoom the result back up to the original image size minus the padding
        zoomed_result = np.squeeze(np.array([resize(result[c], (downsamples[0]*result.shape[-2], downsamples[1]*result.shape[-1]), self._soft, 'edge', preserve_range=True) for c in range(result.shape[0])], dtype="float32"))
        
        # Add the padding, the pixels lost due to the convolution and pooling
        # operations. This should allow the image to allign nicely with the
        # input image.
        if self._soft:
            padded_result = np.pad(zoomed_result, ((0,0), (pads[2], pads[3]), (pads[0], pads[1])),
                                   'constant')
            if msk_data is not None:
                msk_data = np.repeat(msk_data[None], padded_result.shape[0], 0)
                padded_result = padded_result*msk_data
        else:
            padded_result = np.pad(zoomed_result, ((pads[2], pads[3]), (pads[0], pads[1])),
                                   'constant')
            if msk_data is not None:
                padded_result = (padded_result+1)*msk_data
        return padded_result  
       
    def _process_images_patch_based(self, pred_fun, images=None, msk_data=None):
        """
        Process large images on a patch-by-patch basis.
        
        Images are tiled based on the provided stride and the corresponding
        scales. A tiling function is used to transform large images into sets
        of tiles which are then fed per hundred to the prediction function to
        minimize copying overhead. The results are subsequently reshaped into
        the resultant likelihood map. This map is then upsampled to the
        resolution of the original image and padding is applied where
        appropriate.
        
        Note: it is important to select a stride such that the images are fully
        covered, otherwise the right side of the image will contain extra zero
        values.
        """
        # Load image information from the input images if not provided, load
        # mask data.
        if images is None:
            images = [self._get_image_data_from_input(scale=s) for s in self._scales]
        if self._mask is not None:
            if msk_data is None:
                msk_data = self._get_image_data_from_mask()            
                
        # Calculate the approriate strides for each scale                
        strides = [int(self._stride /
                       (float(s) / self._scales[self._output_scale]))
                   for s in self._scales]
        out_image_size = images[self._output_scale].shape
        
        # Tile the image in model_patch_size sized tiles
        result_fill_area = (out_image_size[1] - self._model_patch_size[1],
                            out_image_size[2] - self._model_patch_size[2])
        patches_row = result_fill_area[0] // self._stride + 1
        patches_col = result_fill_area[1] // self._stride + 1
        tls = [utils.image_to_patches(image,
                                      self._model_patch_size,
                                      (self._model_patch_size[0], stride,
                                       stride))
               for image, stride in zip(images, strides)]
                   
        # This code is needed for analysis of images within 1 channel, this
        # channel gets squeezed out in image_to_patches
        if len(tls[0].shape) == 3:
            for i in range(len(tls)):
                tls[i] = tls[i][None].transpose(1,0,2,3)
        
        # Perform the actual classification and reshape the output
        classified_tiles = []
        for i in range(0, len(tls[0]), 100):
            classified_tiles.append(pred_fun(*[t[i:i+100]
                                               for t in tls]))
        classified_tiles = np.concatenate(classified_tiles, axis=0)
        classified_map = classified_tiles.reshape(patches_row, patches_col, -1).transpose(2,0,1)
        
        # Zoom back to the original resolution and apply the appropriate
        # padding.
        zoomed_result = np.squeeze(np.array([resize(classified_map[c],
                                                      (((patches_row - 1) *
                                                       strides[self._output_scale]),
                                                       ((patches_col - 1)*
                                                       strides[self._output_scale])),
                                                      self._soft, 'edge', preserve_range=True) for c in range(classified_map.shape[0])], dtype="float32"))
        if len(zoomed_result.shape) == 3:
            padded_result = np.pad(zoomed_result,
                          ((0,0), (self._model_patch_size[1]//2,
                            images[self._output_scale].shape[1] - zoomed_result.shape[-2] -
                            self._model_patch_size[1]//2), 
                           (self._model_patch_size[2]//2,
                            images[self._output_scale].shape[2] - zoomed_result.shape[-1] -
                            self._model_patch_size[2]//2)),
                          'constant')
            if msk_data is not None:
                msk_data = np.repeat(msk_data[None], padded_result.shape[0], 0)
                padded_result = padded_result*msk_data                          
        else :
            padded_result = np.pad(zoomed_result,
                          ((self._model_patch_size[1]//2,
                            images[self._output_scale].shape[1] - zoomed_result.shape[-2] -
                            self._model_patch_size[1]//2), 
                           (self._model_patch_size[2]//2,
                            images[self._output_scale].shape[2] - zoomed_result.shape[-1] -
                            self._model_patch_size[2]//2)),
                          'constant')
            if msk_data is not None:
                padded_result = (padded_result+1)*msk_data                               
        return padded_result

    ###################################
    # Tile-based processing functions #
    ###################################
    def _calculate_image_offsets(self, img_shape, model):
        """
        Calculates how much more/less pixels are needed for non-output scales.
        
        When doing multi-scale analysis, the image at output scale determines
        the pixel data needed from the other scales to get an output for every
        pixel in the input image (minus the patch size, which you lose anyway).
        
        This functions calculates for a given output scale what offsets are
        needed for the other scales.
        """
        patch_sides = [self._model_patch_size[-2] // 2,
                       ceil(self._model_patch_size[-2] / 2.),
                       self._model_patch_size[-1] // 2,
                       ceil(self._model_patch_size[-1] / 2.)]
        offsets = [[-(patch_sides[0]  - (patch_sides[0] // (s / self._scales[0]))),
                    patch_sides[1]  - (patch_sides[1] // (s / self._scales[0])),
                    -(patch_sides[2]  - (patch_sides[2] // (s / self._scales[0]))),
                    patch_sides[3]  - (patch_sides[3] // (s / self._scales[0]))] for s in self._scales]
        return offsets        
        
    def _calculate_request_tile_size(self, base_shape, scale, offsets):
        """
        Calculates the tile size needed for tile-based processing.
        
        When performing tile based processing, the tile size is increased by
        model_patch_size to prevent artifacts as much as possible. This
        function calculates the correct tile size for the other scales.
        """
        if self._mode == "fully_convolutional":
            height_current = (base_shape[2] - self._model_patch_size[1]) // (float(scale) / self._scales[self._output_scale]) +  self._model_patch_size[1]
            width_current = (base_shape[3] - self._model_patch_size[2]) // (float(scale) / self._scales[self._output_scale]) +  self._model_patch_size[2]
        else:
            strides = [int(self._stride /
                           (float(s) / self._scales[self._output_scale]))
                       for s in self._scales]
            st = strides[self._scales.index(scale)]
            height_current = ((base_shape[2] - self._model_patch_size[1]) // strides[self._output_scale]) * st + self._model_patch_size[1]
            width_current = ((base_shape[3] - self._model_patch_size[2]) // strides[self._output_scale]) * st + self._model_patch_size[2]
        return height_current, width_current
        
    def _process_images_tiled(self, pred_fun, model):
        """
        Classifies an image on a tile-by-tile basis.
        
        Larger images sometimes do not fit completely into GPU imaging. To 
        make sure that they can still be classified we can perform
        classification on a tile-by-tile basis. This causes some difficulties
        as artificats can occur on the tile edges due to miss-alignment of the
        filters compared to analyzing the image in one go. To prevent this as
        much as possible, the user should specify a tile size which is a
        multiple of the model_patch_size. Furthermore, in this function we
        enlarge the tile size with 2*model_patch_size to further prevent edge
        effects. 
        """
        out_scale = self._scales[self._output_scale]
        input_shapes = self._get_input_image_shapes()
        offsets = self._calculate_image_offsets([None] + list(input_shapes[self._output_scale]), model)        
        additional_scales = [x for x in range(len(self._scales))
                             if x != self._output_scale]
        self._initialize_result()
        res_shp = self._get_result_shape()
        row_range = trange(0, res_shp[-2], self._tile_size) if self._verbose else xrange(0, res_shp[-2], self._tile_size)
        for row in row_range:
            col_range = trange(0, res_shp[-1], self._tile_size) if self._verbose else xrange(0, res_shp[-1], self._tile_size)
            for col in col_range:
                left_pad = max(0, self._model_patch_size[2] - col)
                top_pad = max(0, self._model_patch_size[1] - row)
                top_index = max(0, row - self._model_patch_size[1])
                bottom_index = row + self._tile_size + self._model_patch_size[1]
                left_index = max(0, col - self._model_patch_size[2])
                right_index = col + self._tile_size + self._model_patch_size[2]
                msk_data = None
                if self._mask is not None:
                    msk_data = self._get_image_data_from_mask([top_index, bottom_index, left_index, right_index])
                    if not msk_data.any():
                        self._write_tile_to_result(np.zeros((self._model.output_shape[1], self._tile_size, self._tile_size), dtype="float32"), row, col)
                        continue
                base_tile = self._get_image_data_from_input([top_index, bottom_index, left_index, right_index], self._scales[self._output_scale])
                right_pad = self._tile_size + 2*self._model_patch_size[2] - left_pad - base_tile.shape[2]
                bottom_pad = self._tile_size + 2*self._model_patch_size[1] - top_pad - base_tile.shape[1]
                padded_base_tile = np.pad(base_tile, ((0,0), (top_pad, bottom_pad), (left_pad, right_pad)), 'constant')
                if msk_data is not None:
                    msk_data = np.pad(msk_data, ((top_pad, bottom_pad), (left_pad, right_pad)), 'constant')
                tiles = [padded_base_tile] * len(self._scales)
                for asc in additional_scales:
                    sc = self._scales[asc]
                    left_index_current = int(left_index / (sc / float(out_scale)))
                    left_pad_current = int(left_pad / (sc / float(out_scale)))
                    top_index_current = int(top_index / (sc / float(out_scale)))        
                    top_pad_current = int(top_pad / (sc / float(out_scale)))
                    height_current, width_current = self._calculate_request_tile_size(padded_base_tile[None].shape, sc, offsets)
                    box = [top_index_current, top_index_current + height_current - top_pad_current, left_index_current, left_index_current + width_current - left_pad_current]
                    cur_tile = self._get_image_data_from_input(box, sc)
                    bottom_pad_current = int(height_current - top_pad_current - cur_tile.shape[1])
                    right_pad_current = int(width_current - left_pad_current - cur_tile.shape[2])
                    padded_cur_tile = np.pad(cur_tile, ((0,0), (top_pad_current, bottom_pad_current), (left_pad_current, right_pad_current)), 'constant')
                    tiles[asc] = padded_cur_tile              
                for i, tile in enumerate(tiles):
                    img = Image.fromarray((tile.transpose(1,2,0) * 255).astype("ubyte"))
                    img.save(r"d:\\" + "tile_scale" + str(i) + "_x_" + str(col) + "_y_" + str(row) + ".png")
                if self._mode == "fully_convolutional":
                    padded_result = self._process_images_fully_convolutional(pred_fun, tiles, msk_data)
                else:
                    padded_result = self._process_images_patch_based(pred_fun[0], tiles, msk_data)
                if padded_result is None:
                    self._logger.error("Image could not be processed")
                    return
                if len(padded_result.shape) == 3:
                    clipped_result = padded_result[:, self._model_patch_size[-2]:padded_result.shape[-2] - bottom_pad,
                                                      self._model_patch_size[-1]:padded_result.shape[-1] - right_pad]
                else:
                    clipped_result = padded_result[self._model_patch_size[-2]:padded_result.shape[-2] - bottom_pad,
                                                      self._model_patch_size[-1]:padded_result.shape[-1] - right_pad]
                clipped_result = self._postprocess_tile(clipped_result, row, col)
                self._write_tile_to_result(clipped_result, row, col)
        self._finish()
        
    def _postprocess_tile(self, tile, row, col):
      if self._postprocess_function:
        return self._postprocess_function(tile, row, col)
      else:
        return tile
                       
    def _write_tile_to_result(self, tile, row, col):
        """
        Writes a result tile to the result image.
        
        When performing tile-by-tile analysis, tiles are written to the result
        one-by-one. This also allows writing of results to disk in derived
        classes.
        """
        if not self._result is None:
            if self._soft:
                if self._quantize:
                    tile = np.clip(((tile - self._quantize_min) / (self._quantize_max - self._quantize_min)) * 255, 0, 255).astype("ubyte")
                if self._output_class >= 0:
                  self._result[row:row+tile.shape[-2], col:col+tile.shape[-1]] = tile[self._output_class]
                else:
                  self._result[:, row:row+tile.shape[-2], col:col+tile.shape[-1]] = tile
            else:
                self._result[row:row+tile.shape[-2], col:col+tile.shape[-1]] = tile