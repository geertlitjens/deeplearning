import os
import joblib
import time
import numpy as np
import lasagne.layers as L

import multiresolutionimageinterface as mir
from deeplearning.classification.wsi_classifier import WSIImageClassifier

test_fldr = os.path.dirname(os.path.realpath(__file__))

def softmax_tile(tile, row, col):
  e_x = np.exp(tile - tile.max(axis=0, keepdims=True))
  return e_x / e_x.sum(axis=0, keepdims=True)
    
def test_tiled(test_fldr, mode="fully_convolutional"):
    model = joblib.load(os.path.join(test_fldr, "pinsingle_newlasagne.pkl"))
    r = mir.MultiResolutionImageReader()
    im = r.open(str(os.path.join(test_fldr, "PIN_test.ndpi")))
    img_cla = WSIImageClassifier()
    img_cla.set_model(model)
    img_cla.set_mode(mode)
    img_cla.set_model_patch_size([3,128,128])
    img_cla.set_soft(True)
    img_cla.set_tile_size(256)
    img_cla.set_scales([2])
    img_cla.set_stride(8)
    img_cla.set_input(im)
    img_cla.set_quantize(True)
    img_cla.set_postprocess_function(softmax_tile)
    img_cla.set_result_file_path(os.path.join(test_fldr, "PIN_test_label_"+ mode +".tif"))
    img_cla.process()

def test_multiscale_tiled(test_fldr, out_scale=0, mode="fully_convolutional"):
    model = joblib.load(os.path.join(test_fldr, "pinmulti.pkl"))
    img_cla = WSIImageClassifier()    
    r = mir.MultiResolutionImageReader()
    im = r.open(str(os.path.join(test_fldr, "PIN_test.ndpi")))
    img_cla.set_input(im)
    img_cla.set_model(model)   
    img_cla.set_mode(mode)
    img_cla.set_model_patch_size([3,256,256])
    img_cla.set_stride(8)
    img_cla.set_soft(True)
    img_cla.set_tile_size(256)
    img_cla.set_scales([1.,2.,4.])
    img_cla.set_output_scale(out_scale)
    img_cla.set_result_file_path(os.path.join(test_fldr, "PIN_test_label_multiscale_"+ mode +".tif"))
    img_cla.process()

t = time.time()
#test_tiled(test_fldr)
print "Single-scale fully convolutional took: " + str(time.time() - t)
#test_tiled(test_fldr)
t = time.time()
test_multiscale_tiled(test_fldr, 0, "fully_convolutional")
print "Multi-scale fully convolutional took: " + str(time.time() - t)
#test_multiscale_tiled(test_fldr, 1, "patch_based")