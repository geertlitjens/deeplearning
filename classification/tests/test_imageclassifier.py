import os
import joblib
import numpy as np
import time
from deeplearning.classification.image_classifier import ImageClassifier

test_fldr = os.path.dirname(os.path.realpath(__file__))
    
def test_classifier(test_fldr, ptch, model, model_patch_size, mode="fully_convolutional", tile_size=0):   
    img_cla = ImageClassifier()    
    img_cla.set_model(model)
    img_cla.set_mode("fully_convolutional")
    img_cla.set_model_patch_size(model_patch_size)
    img_cla.set_stride(12)
    img_cla.set_soft(True)
    img_cla.set_tile_size(tile_size)
    img_cla.set_input(ptch)
    img_cla.process()
    return img_cla.get_result()  
    
def test_multiscale_classifier(test_fldr, ptch1, ptch2, ptch3, model, model_patch_size, out_scale=0, mode="fully_convolutional", tile_size=0):
    img_cla = ImageClassifier()    
    img_cla.set_model(model)
    img_cla.set_mode(mode)
    img_cla.set_model_patch_size(model_patch_size)
    img_cla.set_stride(12)
    img_cla.set_soft(True)
    img_cla.set_scales([1,2,4])
    img_cla.set_tile_size(tile_size)
    img_cla.set_output_scale(out_scale)
    img_cla.set_input([ptch1, ptch2, ptch3])
    img_cla.process()
    return img_cla.get_result()
    
ptch1 = np.load(os.path.join(test_fldr,  "patch_scale1.npz"))['arr_0']
ptch2 = np.load(os.path.join(test_fldr,  "patch_scale2.npz"))['arr_0']
ptch3 = np.load(os.path.join(test_fldr,  "patch_scale3.npz"))['arr_0']
single_model = joblib.load(os.path.join(test_fldr, "HE_60_fullcnn.pkl"))
multi_model = joblib.load(os.path.join(test_fldr, "HE_multi_35.pkl"))

s_ptch1 = ptch1[:, 512-256:512+256, 512-256:512+256]
s_ptch2 = ptch2[:, 512-256:512+256, 512-256:512+256]
s_ptch3 = ptch3[:, 512-256:512+256, 512-256:512+256]
s_ptch4 = ptch2[:, 512-128-32:512+128+32, 512-128-32:512+128+32]
s_ptch5 = ptch3[:, 512-64-48:512+64+48, 512-64-48:512+64+48]

t = time.time()
r_ss1 = test_classifier(test_fldr, s_ptch1, single_model, [3, 128, 128])
print "Single-scale fully convolutional took: " + str(time.time() - t)
#t = time.time()
#r_ss2 = test_classifier(test_fldr, s_ptch1, single_model, [3, 128, 128], "patch_based")
print "Single-scale patch-based took: " + str(time.time() - t)
#r_ss3 = test_classifier(test_fldr, s_ptch1, single_model, [3, 128, 128], tile_size=512)
#r_ss4 = test_classifier(test_fldr, s_ptch1, single_model, [3, 128, 128], mode="patch_based", tile_size=512)
t = time.time()
#r_ms1 = test_multiscale_classifier(test_fldr, s_ptch1, s_ptch4, s_ptch5, multi_model, [3, 128, 128], 0)
print "Multi-scale fully convolutional took: " + str(time.time() - t)
#t = time.time()
#r_ms2 = test_multiscale_classifier(test_fldr, s_ptch1, s_ptch4, s_ptch5, multi_model, [3, 128, 128], 0, "patch_based")
print "Multi-scale patch-based took: " + str(time.time() - t)
#r_ms3 = test_multiscale_classifier(test_fldr,  ptch1, ptch2, ptch3, multi_model, [3, 128, 128], 1, mode="fully_convolutional", tile_size=256)
#r_ms4 = test_multiscale_classifier(test_fldr, ptch1, ptch2, ptch3, multi_model, [3, 128, 128], 1, mode="patch_based", tile_size=256)