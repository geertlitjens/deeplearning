import numpy as np
from image_classifier import ImageClassifier
import deeplearning.utils
import collections
from tqdm import trange

import multiresolutionimageinterface as mir

class WSIImageClassifier(ImageClassifier):
    """
    Classifies whole slide images from disk.
    
    This class applies a fully convolutional model in a patch-based or fully-
    convolutional fashion to a whole-slide image on disk. It will only function
    in a tile-by-tile fashion as whole slide images can not be read into
    memory entirely in general. 
    
    Note: the mask image is always assumed to have the same size as the
    original image at the level corresponding to output scale.
    """    
    def __init__(self):
        super(WSIImageClassifier, self).__init__()
        self._result_file_path = ""
        self._writer = None
        self._channels_in_result = None

    def _check_parameters(self):
        """
        This function checks whether all the parameters are provided correctly.
        
        Small extension to base class functionality as WSIImageClassifier only
        supports tiled analysis.
        """
        checked = super(WSIImageClassifier, self)._check_parameters()
        if self._tile_size < 1:
            self._logger.error(("WSIImageClassifier only supports tiled "
                                " analysis."))
            return False
        return checked
        
    def set_input(self, images, mask=None, mask_level=0):
        """
        Provide the input image(s) for the ImageClassifier to process
        
        In the multi-resolution case, images can be provided as having the same
        size with the same center pixel or cropped to the appropriate size with
        the same center pixel. In the former case, the classifier will
        determine the correct areas to use.
        A mask image is optional, this masks out all zero values of the mask in
        the result image. Mask is expected to be of the same size as the image
        at the output_scale.
        """
        self._input = images
        if mask:
            self._mask = deeplearning.utils.InfiniteMultiResolutionImage(mask)
        else:
            self._mask = None
        self._mask_level = mask_level
        if not isinstance(self._input, collections.Sequence):
            self._input = [deeplearning.utils.InfiniteMultiResolutionImage(self._input)]
        else:
            self._input = [deeplearning.utils.InfiniteMultiResolutionImage(x) for x in self._input]
        self._input_crops = [[0, shp[-2], 0, shp[-1]] for shp in self._get_input_image_shapes()]        
        
    def set_result_file_path(self, pth):
        """
        Set the path to the result file on disk.
        """
        self._result_file_path = pth
    
    def get_result_file_path(self):
        """
        Returns the path to the result file on disk.
        """
        return self._result_file_path
                  
    def _get_input_image_shapes(self):
        """
        Get input image shapes for differing scales.
        
        In a WSIImage, the different scales are within the same image file at
        different levels. As such we acquire the dimensions at the different
        levels.
        """
        if self._input:
            levels = [self._input[0].getBestLevelForDownSample(s) for s in self._scales]
            return [[3] + list(reversed(self._input[0].getLevelDimensions(l))) for l in levels]

    def _get_mask_shape(self):
        """
        Get the mask dimensions.
        """
        if self._mask is not None:
            return np.array(list(reversed(self._mask.getLevelDimensions(self._mask_level))))
        else :
            return np.array()
                     
    def _get_image_data_from_input(self, box=[], scale=1):
        """
        Get pixel data from the image region defined by box at specified scale.
        
        Corrects the box specified for the corresponding image level the data
        has to be acquired from. Then obtain the data from the image file and
        return it.
        """
        lev_nr = self._input[0].getBestLevelForDownSample(scale)
        first_lev_nr = self._input[0].getBestLevelForDownSample(self._scales[0])
        downs = self._input[0].getLevelDownsample(lev_nr)
        downs_base = self._input[0].getLevelDownsample(first_lev_nr)
        crop_downs = downs / downs_base
        crop = self._input_crops[0]
        box[0] = box[0] + crop[0] / crop_downs
        box[1] = box[1] + crop[0] / crop_downs if box[1] + crop[0] / crop_downs <= crop[1] / crop_downs else crop[1] / crop_downs
        box[2] = box[2] + crop[2] / crop_downs
        box[3] = box[3] + crop[2] / crop_downs if box[3] + crop[2] / crop_downs <= crop[3] / crop_downs else crop[3] / crop_downs
        height = int(box[1] - box[0])
        width = int(box[3] - box[2])
        tl = self._input[0].getUCharPatch(int(box[2]*downs),
                                          int(box[0]*downs),
                                          int(width), int(height), int(lev_nr))
        return (self._preprocess_tile(tl, self._lut).transpose(2,0,1) / 255.).astype("float32")
        
    def _get_image_data_from_mask(self, box=[]):
        """
        Get mask data from the image file from the area defined by box.
        """
        if self._mask is not None:
            downs = self._mask.getLevelDownsample(self._mask_level)
            crop = self._input_crops[self._output_scale]
            box[0] = box[0] + crop[0]
            box[1] = box[1] + crop[0] if box[1] + crop[0] <= crop[1] else crop[1]
            box[2] = box[2] + crop[2]
            box[3] = box[3] + crop[2] if box[3] + crop[2] <= crop[3] else crop[3]
            height = int(box[1] - box[0])
            width = int(box[3] - box[2])
            tl = self._mask.getUCharPatch(int(box[2]*downs),
                                          int(box[0]*downs),
                                          int(width), int(height), self._mask_level)
            return np.squeeze(tl)
        return np.array()
      
    def _initialize_result(self):
        """
        Initializes the result image on disk based on provided parameters.
        
        Instantiates the image file on disk and sets attributes like data type,
        number of channels and color type.
        """
        self._writer = mir.MultiResolutionImageWriter()
        nr_channels = 1
        if self._soft and self._output_class < 0:
            nr_channels = self._model.output_shape[1]
        self._writer.openFile(str(self._result_file_path))
        self._writer.setTileSize(self._tile_size)
        self._writer.setCompression(mir.LZW)

        spacing = self._input[0].getSpacing()
        if spacing:
            downsample = self._scales[0]
            spacing_vec = mir.vector_double()
            spacing_vec.push_back(spacing[0] * downsample)
            spacing_vec.push_back(spacing[1] * downsample)
            self._writer.setSpacing(spacing_vec)

        if self._soft:
            if self._quantize:
                self._writer.setDataType(mir.UChar)
            else:
                self._writer.setDataType(mir.Float)
            self._writer.setColorType(mir.Indexed)
            self._writer.setNumberOfIndexedColors(nr_channels)
        else:
            self._writer.setDataType(mir.UChar)
            self._writer.setInterpolation(mir.NearestNeighbor)
            self._writer.setColorType(mir.Monochrome)
        shp = self.get_input_image_crops()[self._output_scale]
        self._channels_in_result = nr_channels
        self._writer.writeImageInformation(int(shp[3]-shp[2]), int(shp[1]-shp[0]))
        return
    
    def _get_result_shape(self):
        """
        Returns the shape of the result image.
        """
        if not self._writer is None:
            crops = self.get_input_image_crops()[self._output_scale]
            shape = [crops[1] - crops[0], crops[3]- crops[2]]
            return  [self._channels_in_result] + [int(x) for x in shape]
            
    def _process_images_tiled(self, pred_fun, model):
        """
        Classifies an image on a tile-by-tile basis.
        
        Larger images sometimes do not fit completely into GPU imaging. To 
        make sure that they can still be classified we can perform
        classification on a tile-by-tile basis. This causes some difficulties
        as artificats can occur on the tile edges due to miss-alignment of the
        filters compared to analyzing the image in one go. To prevent this as
        much as possible, the user should specify a tile size which is a
        multiple of the model_patch_size. Furthermore, in this function we
        enlarge the tile size with 2*model_patch_size to further prevent edge
        effects. 
        """
        out_scale = self._scales[self._output_scale]
        additional_scales = [x for x in range(len(self._scales))
                             if x != self._output_scale]
        self._initialize_result()
        res_shp = self._get_result_shape()
        for row in trange(0, res_shp[-2], self._tile_size):
            for col in trange(0, res_shp[-1], self._tile_size):
                lev_nr = self._input[0].getBestLevelForDownSample(out_scale)
                downs = self._input[0].getLevelDownsample(lev_nr)
                bs_col = col  - self._model_patch_size[-1] / 2
                bs_row = row  - self._model_patch_size[-2] / 2
                msk_data = None
                if self._mask is not None:
                    msk_data = np.squeeze(self._mask.getUCharPatch(int(bs_col*downs),
                                                  int(bs_row*downs),
                                                  int(self._tile_size + self._model_patch_size[-2]), int(self._tile_size + self._model_patch_size[-1]), int(self._mask_level)))
                    if not msk_data.any():
                        self._write_tile_to_result(np.zeros((self._model.output_shape[1], self._tile_size, self._tile_size), dtype="float32"), row, col)
                        continue                
                base_tile = (self._input[0].getUCharPatch(int(bs_col*downs),
                                                  int(bs_row*downs),
                                                  int(self._tile_size + self._model_patch_size[-2]), int(self._tile_size + self._model_patch_size[-1]), int(lev_nr)).transpose(2,0,1) / 255.).astype("float32")
                base_tile = self._preprocess_tile(base_tile, self._lut)
                tiles = [base_tile] * len(self._scales)
                for asc in additional_scales:
                    sc = self._scales[asc]
                    cur_level = self._input[0].getBestLevelForDownSample(sc)
                    cur_downs = self._input[0].getLevelDownsample(cur_level)
                    conv_fact = (self._scales[self._output_scale] / sc)
                    width = 2 * ((((self._tile_size + self._model_patch_size[-2]) / 2 - self._model_patch_size[-2] / 2) * conv_fact) + self._model_patch_size[-2] / 2)
                    height = 2 * ((((self._tile_size + self._model_patch_size[-2]) / 2 - self._model_patch_size[-1] / 2) * conv_fact) + self._model_patch_size[-1] / 2)                    
                    cur_col = ((bs_col + self._model_patch_size[-2] / 2) * conv_fact) - self._model_patch_size[-2] / 2
                    cur_row = ((bs_row + self._model_patch_size[-2] / 2) * conv_fact) - self._model_patch_size[-1] / 2
                    cur_tile = (self._input[0].getUCharPatch(int(cur_col*cur_downs),
                                                            int(cur_row*cur_downs),
                                                            int(width), int(height), int(cur_level)).transpose(2,0,1) / 255.).astype("float32")
                    tiles[asc] = cur_tile              
                if self._mode == "fully_convolutional":
                    padded_result = self._process_images_fully_convolutional(pred_fun, tiles, None)
                else:
                    padded_result = self._process_images_patch_based(pred_fun[0], tiles, None)
                clipped_result = self._postprocess_tile(padded_result, row, col)
                if msk_data is not None:
                    clipped_result *= (msk_data > 0)
                if len(padded_result.shape) == 3:
                    clipped_result = clipped_result[:, self._model_patch_size[-2] // 2:-(self._model_patch_size[-2] // 2), self._model_patch_size[-2] // 2:-(self._model_patch_size[-2] // 2)]
                else:
                    clipped_result = clipped_result[self._model_patch_size[-2] // 2:-(self._model_patch_size[-2] // 2), self._model_patch_size[-2] // 2:-(self._model_patch_size[-2] // 2)]
                self._write_tile_to_result(clipped_result, row, col)
        self._finish()            
       
    def _write_tile_to_result(self, tile, row, col):
        """
        Write a single tile to the result image.
        """
        if not self._writer is None:
            if tile.shape[-1] < self._tile_size or tile.shape[-2] < self._tile_size:
                if self._soft:
                    tile = np.pad(tile, ((0,0), (0, max(0,self._tile_size - tile.shape[-2])), (0, max(0,self._tile_size - tile.shape[-1]))), 'constant')                  
                else:
                    tile = np.pad(tile, ((0, max(0, self._tile_size - tile.shape[-2])), (0, max(0, self._tile_size - tile.shape[-1]))), 'constant')
            if self._soft:
                if self._output_class < 0:
                  write_tile = tile[:,0:self._tile_size, 0:self._tile_size].transpose(1,2,0).flatten()
                else:
                  write_tile = tile[self._output_class,0:self._tile_size, 0:self._tile_size].flatten()
                if self._quantize:
                    write_tile = np.clip(((write_tile - self._quantize_min) / (self._quantize_max - self._quantize_min)) * 255, 0, 255).astype("ubyte")
                self._writer.writeBaseImagePart(write_tile)
            else:
                self._writer.writeBaseImagePart(tile[0:self._tile_size, 0:self._tile_size].astype("ubyte").flatten())
    
    def _finish(self):
        """
        Write the result image pyramid and close the file.
        """
        if self._writer:
            self._writer.finishImage()