# -*- coding: utf-8 -*-
"""
Created on Wed Sep 02 17:18:13 2015

@author: Geert
"""

import numpy as np

from deeplearning import BatchPreprocessor
from deeplearning import BatchPostprocessor

class OnlineBooster(BatchPreprocessor, BatchPostprocessor):
    """
    Keeps track of difficult examples and augments batches with these examples.
    
    The OnlineBooster main functions are preprocess_batch and 
    postprocess_batch. In preprocess_batch it replaces random examples 
    from the input batch with more difficult samples. In postprocess_batch
    the resultant classification of a batch is used to extract the difficult 
    samples. Currently, the difficult samples are determined by simply
    including all samples which are wrongly classified. This can be changed
    by adapting the threshold required_error.
    
    The amount of samples replaced is determined via rho and alpha, which are
    updated online. The batch size of the input times rho is the number of
    samples replaced. Rho is updated every 1000 batches via:
    
    rho = alpha * (avg_acc - 1.0/nr_classes)
    
    where avg_acc is the accuracy across the last 1000 batches. The 
    1.0 / nr_classes is included for the guess accuracy given the number of
    classes, i.e. when you have a two class problem and your accuracy is 0.5, 
    no difficult samples are needed as you are guessing.
    
    The other parameters of this class are:
    - balanced: should we replace the same amount of positive and negative 
                samples?
    - samples_to_cache: how many samples should we keep track of at one time?
                        If this amount is exceeded, older samples are removed.
    - only_negatives: for two-class problems, only boost class 0. Usefull when
                      annotations for class 1 are unreliable.
    """
    def __init__(self, rho=0.0, alpha=0.5, required_error=0.5,
                 balanced=True, samples_to_cache=10000,
                 only_negatives=False):
        self._rho = rho
        self._alpha = alpha
        self._required_error = required_error
        self._balanced = balanced
        self._boosting_samples_images = None
        self._boosting_samples_labels = None
        self._filled_index = 0
        self._removal_index = 0
        self._samples_to_cache = samples_to_cache
        self._avg_accuracy = 0.0
        self._batches_since_parameter_update = 0
        self._nr_batches_before_parameter_update = 1000
        self._only_negatives = only_negatives
    
    def preprocess_batch(self, batch):
        if self._rho != 0.0:
            nr_replacements = int(batch[0].shape[0] * self._rho)
            if nr_replacements < self._filled_index:
                boosted_samples_indices = np.random.randint(0,self._filled_index, nr_replacements)
                if self._only_negatives:
                    replaced_samples_indices = np.random.permutation(np.where(np.equal(np.argmax(batch[1], axis=1),0))[0])[:nr_replacements]
                else:
                    replaced_samples_indices = np.random.choice(batch[0].shape[0], nr_replacements, replace=False)
                if len(boosted_samples_indices) > len(replaced_samples_indices):
                    boosted_samples_indices = boosted_samples_indices[:len(replaced_samples_indices)]
                batch[0][replaced_samples_indices] = self._boosting_samples_images[boosted_samples_indices]
                batch[1][replaced_samples_indices] = self._boosting_samples_labels[boosted_samples_indices]
        return batch
   
    def postprocess_batch(self, batch, predictions=None):
        if predictions is None:
            print "Booster requires predictions, but they are not provided"
            return batch
            
        # Calculate batch accuracy
        class_labels = np.argmax(batch[1], axis=1)
        class_likelihoods = predictions[np.arange(len(predictions)),
                                        class_labels]
                                        
        self._avg_accuracy += np.mean(np.equal(np.argmax(predictions, axis=1),
                                              class_labels))
        self._batches_since_parameter_update += 1
        
        # Recalculate boosting percentage
        if self._batches_since_parameter_update == self._nr_batches_before_parameter_update:
            self._avg_accuracy /= self._nr_batches_before_parameter_update
            self._rho = self._alpha * (self._avg_accuracy - 1.0/batch[1].shape[1])
            print "Boosting parameters updated. Average accuracy was " + str(self._avg_accuracy)
            print "New rho = " + str(self._rho)
            if self._rho > 1.0:
                self._rho = 1.0
            if self._rho < 0.0:
                self._rho = 0.0
            self._avg_accuracy = 0.0
            self._batches_since_parameter_update = 0
                                        
        # Select images
        indices = np.where(class_likelihoods < (1.-self._required_error))
        if self._only_negatives:
            indices = indices[0][np.where(class_labels[indices] == 0)[0]]
        sel_imgs = batch[0][indices]
        sel_labs = batch[1][indices]
        if self._boosting_samples_images is None:
            self._boosting_samples_images = np.zeros((self._samples_to_cache, batch[0].shape[1], batch[0].shape[2], batch[0].shape[3]), dtype="float32")
            self._boosting_samples_labels = np.zeros((self._samples_to_cache, batch[1].shape[1]), dtype="float32")
        
        # Check if we have to remove samples first
        if self._filled_index == self._samples_to_cache:
            if self._removal_index + sel_imgs.shape[0] <= self._samples_to_cache:
                self._boosting_samples_images[self._removal_index:self._removal_index + sel_imgs.shape[0]] = sel_imgs
                self._boosting_samples_labels[self._removal_index:self._removal_index + sel_labs.shape[0]] = sel_labs
                self._removal_index += sel_imgs.shape[0]
            else:
                to_end = self._samples_to_cache - self._removal_index
                from_begin = sel_imgs.shape[0] - to_end
                self._boosting_samples_images[self._removal_index:] = sel_imgs[0:to_end]
                self._boosting_samples_images[:from_begin] = sel_imgs[to_end:]
                self._boosting_samples_labels[self._removal_index:] = sel_labs[0:to_end]
                self._boosting_samples_labels[:from_begin] = sel_labs[to_end:]                
                self._removal_index = from_begin
        elif self._filled_index + sel_imgs.shape[0] > self._samples_to_cache:
            to_end = self._samples_to_cache - self._filled_index
            from_begin = sel_imgs.shape[0] - to_end
            self._boosting_samples_images[self._filled_index:] = sel_imgs[0:to_end]
            self._boosting_samples_images[:from_begin] = sel_imgs[to_end:]
            self._boosting_samples_labels[self._filled_index:] = sel_labs[0:to_end]
            self._boosting_samples_labels[:from_begin] = sel_labs[to_end:]                 
            self._filled_index = self._samples_to_cache
            self._removal_index = from_begin
        else :
            self._boosting_samples_images[self._filled_index:self._filled_index + sel_imgs.shape[0]] = sel_imgs
            self._boosting_samples_labels[self._filled_index:self._filled_index + sel_labs.shape[0]] = sel_labs
            self._filled_index += sel_imgs.shape[0]
        return batch