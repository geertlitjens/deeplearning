__all__ = ["utils", "data_handling", "augmentation",
           "training_extensions", "classification", "visualization"]
                      
class BatchPreprocessor(object):
    """ 
    Base class definition for preprocessing batches
    
    Defines the main function of a preprocessor: preprocess_batch, which
    modifies a batch inplace.
    """
    def __init__(self):
        pass
    
    def preprocess_batch(self, batch):
        return batch

class BatchPostprocessor(object):
    """ 
    Base class definition for postprocessing batches
    
    Defines the main function of a postprocessor: postprocess_batch, which
    modifies a batch inplace. It can optionally use the predictions of a 
    network.
    """    
    def __init__(self):
        pass
    
    def postprocess_batch(self, batch, predictions=None):
        return batch