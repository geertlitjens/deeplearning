import numpy as np
from skimage.transform import resize
from numpy.lib.stride_tricks import as_strided as ast
import multiresolutionimageinterface as mir
 
class InfiniteMultiResolutionImage(object):
    def __init__(self, img):
        self._img = img                
        self._nr_real_levels = self._img.getNumberOfLevels()        
        self._last_level = self._nr_real_levels - 1
        self._last_level_dims = img.getLevelDimensions(self._last_level)
        self._nr_virtual_levels = int(np.floor(np.log2(np.min(self._last_level_dims))))
        self._total_levels = self._nr_real_levels + self._nr_virtual_levels
        self._level_dimensions = [list(self._img.getLevelDimensions(x)) for x in xrange(self._nr_real_levels)]
        for i in xrange(self._nr_virtual_levels):
            self._level_dimensions.append(tuple([x // 2 for x in self._level_dimensions[-1]]))
        self._level_downsamples = [self._img.getLevelDownsample(x) for x in xrange(self._nr_real_levels)]
        for i in xrange(self._nr_virtual_levels):
            self._level_downsamples.append(self._level_downsamples[-1] * 2)
        
    def getNumberOfLevels(self):
        return self._img.getNumberOfLevels() + self._nr_virtual_levels
    
    def getLevelDimensions(self, level):
        return self._level_dimensions[level]
        
    def getLevelDownsample(self, level):
        return self._level_downsamples[level]
        
    def getBestLevelForDownSample(self, downsample):
        previous_downSample = 1.0;
        if (downsample < 1.0):
            return 0;
        for i in xrange(len(self._level_dimensions)):
            current_downSample = self._level_dimensions[0][0] / self._level_dimensions[i][0]
            previous_downSample = self._level_dimensions[0][0] / self._level_dimensions[i-1][0]
            if (downsample < current_downSample):            
                if (np.abs(current_downSample - downsample) > np.abs(previous_downSample - downsample)):
                    return i - 1
                else:
                    return i
        return self._total_levels - 1;
    
    def getMinValue(self, channel=-1):
        return self._img.getMinValue(channel)
        
    def getMaxValue(self, channel=-1):
        return self._img.getMaxValue(channel)
        
    def getSpacing(self):
        return self._img.getSpacing()
    
    def close(self):
        self._img.close()
        
    def _getPatch(self, x, y, width, height, level, patch_getter):
        if x < 0:
            downs = self.getLevelDownsample(level)
            pad_left = int(np.round(-1 * (x / downs)))
            req_width = width - pad_left
            req_x = 0
        else:
            req_x = x
            req_width = width
            pad_left = 0
        if y < 0:
            downs = self.getLevelDownsample(level)
            pad_top = int(np.round(-1 * (y / downs)))
            req_height = height - pad_top            
            req_y = 0
        else:
            req_y = y
            req_height = height
            pad_top = 0
        if level < self._nr_real_levels:
            ptch = patch_getter(int(req_x), int(req_y), int(req_width), int(req_height), int(level))
        else:
            correction_factor = 2 ** (level - self._nr_real_levels + 1)
            ptch = patch_getter(int(req_x), int(req_y), int(req_width * correction_factor), int(req_height * correction_factor), int(self._nr_real_levels - 1))
            if self._img.getColorType() == mir.Monochrome:
                ptch = resize(ptch, (req_height, req_width), preserve_range=True, order = 0)
            else:
                ptch = resize(ptch, (req_height, req_width), preserve_range=True, order = 1)
        if pad_left > 0 or pad_top > 0:
            if len(ptch.shape) == 3:
                ptch = np.pad(ptch, ((pad_top, 0), (pad_left, 0), (0, 0)), "constant")
            else:
                ptch = np.pad(ptch, ((pad_top, 0), (pad_left, 0)), "constant")                
        return ptch
        
    def getUCharPatch(self, x, y, width, height, level):
        ptch = self._getPatch(x, y, width, height, level, self._img.getUCharPatch)             
        return ptch.astype("ubyte")
    
    def getFloatPatch(self, x, y, width, height, level):
        ptch = self._getPatch(x, y, width, height, level, self._img.getFloatPatch)             
        return ptch.astype("float32")
        
def norm0255(array_to_normalize):
    """
    Maps the values of an array to a range of 0 - 255, return uint8 array
    """
    arr = array_to_normalize.copy()
    arr -= arr.min()
    arr *= 255.0 / (arr.max() + 1e-10)
    arr = np.array(arr, 'uint8')
    return arr
    
def norm01(array_to_normalize):
    """
    Map the values of an array to a range of 0 - 1, return float array
    """    
    arr = array_to_normalize.copy()
    arr -= arr.min()
    arr /= (arr.max() + 1e-10) #small constant for numerical stability
    arr = np.array(arr, 'float32')
    return arr


def image_to_patches(a, ws, ss=None, flatten=True):
    '''
    Converts an arbitray n-dimensional image into patches
     
    Parameters:
        a  - an n-dimensional numpy array
        ws - an int (a is 1D) or tuple (a is 2D or greater) representing the 
             size of each dimension of the window
        ss - an int (a is 1D) or tuple (a is 2D or greater) representing the 
             amount to slide the window in each dimension. If not specified, it
             defaults to ws.
        flatten - if True, all slices are flattened, otherwise, there is an 
                  extra dimension for each dimension of the input.
     
    Returns
        an array containing each n-dimensional window from a
    '''
    
    def norm_shape(shape):
        try:
            i = int(shape)
            return (i,)
        except TypeError:
            # shape was not a number
            pass
     
        try:
            t = tuple(shape)
            return t
        except TypeError:
            # shape was not iterable
            pass     
     
    if None is ss:
        # ss was not provided. the windows will not overlap in any direction.
        ss = ws
    ws = norm_shape(ws)
    ss = norm_shape(ss)
     
    # convert ws, ss, and a.shape to numpy arrays so that we can do math
    # in every dimension at once.
    ws = np.array(ws)
    ss = np.array(ss)
    shape = np.array(a.shape)
     
     
    # ensure that ws, ss, and a.shape all have the same number of dimensions
    ls = [len(shape),len(ws),len(ss)]
    if 1 != len(set(ls)):
        raise ValueError(\
        'a.shape, ws and ss must have the same length. They were %s' % str(ls))
     
    # ensure that ws is smaller than a in every dimension
    if np.any(ws > shape):
        raise ValueError(\
        'ws cannot be larger than a in any dimension.\
         a.shape was %s and ws was %s' % (str(a.shape),str(ws)))
     
    # how many slices will there be in each dimension?
    newshape = norm_shape(((shape - ws) // ss) + 1)
    # the shape of the strided array will be the number of slices 
    # in each dimension plus the shape of the window (tuple addition)
    newshape += norm_shape(ws)
    # the strides tuple will be the array's strides multiplied by step size,
    # plus the array's strides (tuple addition)
    newstrides = norm_shape(np.array(a.strides) * ss) + a.strides
    strided = ast(a,shape = newshape,strides = newstrides)
    if not flatten:
        return strided
     
    # Collapse strided so that it has one more dimension than the window.
    #   I.e., the new array is a flat list of slices.
    meat = len(ws) if ws.shape else 0
    firstdim = (np.product(newshape[:-meat]),) if ws.shape else ()
    dim = firstdim + (newshape[-meat:])
    # remove any dimensions with size 1
    dim = filter(lambda i : i != 1,dim)
    return strided.reshape(dim)
    
def filter_array_to_grid(filters, padsize=1, padval=0):
    # take an array of shape (n, width, height) or (n, channels, width, height)
    # and visualize each (height, width) thing in a grid of size approx. sqrt(n) by sqrt(n)
    n = int(np.ceil(np.sqrt(filters.shape[0])))
    padding = ((0, n ** 2 - filters.shape[0]),) + (((0, 0),) * (filters.ndim - 3)) + ((padsize, padsize), (padsize, padsize))
    data = np.pad(filters, padding, mode='constant', constant_values=(padval, padval))
    
    # tile the filters into an image
    if data.ndim > 3:
        data = data.transpose(0, 2, 3, 1)
    data = data.reshape((n, n) + data.shape[1:]).transpose((0, 2, 1, 3) + tuple(range(4, data.ndim + 1)))
    data = data.reshape((n * data.shape[1], n * data.shape[3]) + data.shape[4:])
    return data
    
def patches_to_images(X, img_shape, tile_shape, tile_spacing=(0, 0),
                      scale_rows_to_unit_interval=True,
                      output_pixel_vals=True):
    """
    Transform an array with one flattened image per row, into an array in
    which images are reshaped and layed out like tiles on a floor.

    This function is useful for visualizing datasets whose rows are images,
    and also columns of matrices for transforming those rows
    (such as the first layer of a neural net).

    :type X: a 2-D ndarray or a tuple of 4 channels, elements of which can
    be 2-D ndarrays or None;
    :param X: a 2-D array in which every row is a flattened image.

    :type img_shape: tuple; (height, width)
    :param img_shape: the original shape of each tile

    :type tile_shape: tuple; (rows, cols)
    :param tile_shape: the number of tiles in each dimension (rows, cols)

    :type tile_spacing: tuple; (row_spacing, col_spacing)
    :param tile_spacing: the spacing in pixels between each tile

    :param output_pixel_vals: if output should be pixel values (i.e. int8
    values) or floats

    :param scale_rows_to_unit_interval: if the values need to be scaled before
    being plotted to [0,1] or not


    :returns: array suitable for viewing as an image.
    (See:`Image.fromarray`.)
    :rtype: a 2-d array with same dtype as X.

    """

    assert len(img_shape) == 2
    assert len(tile_shape) == 2
    assert len(tile_spacing) == 2

    # The expression below can be re-written in a more C style as
    # follows :
    #
    # out_shape    = [0,0]
    # out_shape[0] = (img_shape[0]+tile_spacing[0])*tile_shape[0] -
    #                tile_spacing[0]
    # out_shape[1] = (img_shape[1]+tile_spacing[1])*tile_shape[1] -
    #                tile_spacing[1]
    out_shape = [
        (ishp + tsp) * tshp - tsp
        for ishp, tshp, tsp in zip(img_shape, tile_shape, tile_spacing)
    ]

    if isinstance(X, tuple):
        assert len(X) == 4
        # Create an output np ndarray to store the image
        if output_pixel_vals:
            out_array = np.zeros((out_shape[0], out_shape[1], 4),
                                    dtype='uint8')
        else:
            out_array = np.zeros((out_shape[0], out_shape[1], 4),
                                    dtype=X.dtype)

        #colors default to 0, alpha defaults to 1 (opaque)
        if output_pixel_vals:
            channel_defaults = [0, 0, 0, 255]
        else:
            channel_defaults = [0., 0., 0., 1.]

        for i in xrange(4):
            if X[i] is None:
                # if channel is None, fill it with zeros of the correct
                # dtype
                dt = out_array.dtype
                if output_pixel_vals:
                    dt = 'uint8'
                out_array[:, :, i] = np.zeros(
                    out_shape,
                    dtype=dt
                ) + channel_defaults[i]
            else:
                # use a recurrent call to compute the channel and store it
                # in the output
                out_array[:, :, i] = patches_to_images(
                    X[i], img_shape, tile_shape, tile_spacing,
                    scale_rows_to_unit_interval, output_pixel_vals)
        return out_array

    else:
        # if we are dealing with only one channel
        H, W = img_shape
        Hs, Ws = tile_spacing

        # generate a matrix to store the output
        dt = X.dtype
        if output_pixel_vals:
            dt = 'uint8'
        out_array = np.zeros(out_shape, dtype=dt)

        for tile_row in xrange(tile_shape[0]):
            for tile_col in xrange(tile_shape[1]):
                if tile_row * tile_shape[1] + tile_col < X.shape[0]:
                    this_x = X[tile_row * tile_shape[1] + tile_col]
                    this_img = this_x.reshape(img_shape)
                    # add the slice to the corresponding position in the
                    # output array
                    c = 1
                    if output_pixel_vals:
                        c = 255
                    out_array[
                        tile_row * (H + Hs): tile_row * (H + Hs) + H,
                        tile_col * (W + Ws): tile_col * (W + Ws) + W
                    ] = this_img * c
        return out_array

def get_receptive_field_sizes(layer):
    """
    Calculate receptive field size for Lasagne layer
    
    The receptive field size of a filter is the number of pixels in each
    dimension it covers in the input space. As an example, if you have a 
    network of two layers, 5x5 filters in the first 3x3 filters in the second,
    the receptive field size of the filters in the second layer is 7x7, as 
    7 pixels from the input image in each dimension influence the result
    of the second layer filters.
    
    Currently this function only supports straightforward networks (1 input to
    1 output).  Receptive field size can be calculated as follows:
    - Start with size of the first filter, set downsample to 1
    - For each subsequent layer
       - Add the (filter_size - 1) * downsample (also for pooling layers!)
       - If layer has a stride, multiply downsample with stride
       - If fully connected set field of view to training_patch_size
    """
    import lasagne
    import lasagne.layers.dnn
    import deeplearning.lasagne_extensions.softmax_layer as dlex_sml
    
    ls = lasagne.layers.get_all_layers(layer)
    for l in ls:
        if not isinstance(l, (lasagne.layers.InputLayer,
                              lasagne.layers.Pool2DLayer,
                              lasagne.layers.Conv2DLayer,
                              lasagne.layers.special.NonlinearityLayer,
                              lasagne.layers.noise.DropoutLayer,
                              lasagne.layers.normalization.BatchNormLayer,
                              lasagne.layers.merge.ElemwiseSumLayer,
                              dlex_sml.Softmax4D,
                              lasagne.layers.dnn.Conv2DDNNLayer)):
            print str(l) + " is a non-supported layer."
    base_size = np.array([1,1])
    downsample = np.array([1,1])
    receptive_flds = []
    for l in ls:
        if (not isinstance(l, lasagne.layers.InputLayer) and
            not isinstance(l, lasagne.layers.special.NonlinearityLayer) and
            not isinstance(l, lasagne.layers.noise.DropoutLayer) and
            not isinstance(l, lasagne.layers.normalization.BatchNormLayer) and
            not isinstance(l, dlex_sml.Softmax4D)):
            if isinstance(l, (lasagne.layers.Conv2DLayer,
                              lasagne.layers.dnn.Conv2DDNNLayer)):
                base_size += (np.array(l.filter_size) - 1) * downsample
            elif isinstance(l, lasagne.layers.Pool2DLayer):
                base_size += (np.array(l.pool_size) - 1) * downsample
            downsample *= np.array(l.stride)
        receptive_flds.append(base_size.copy())
    return receptive_flds

def get_reconstruction_information(layer, input_image_shape, separate=False):
    """
    Calculate the scale factor and padding to reconstruct the input shape
    
    This function calculates the information needed to reconstruct the input
    image shape given the output of a layer. For each layer leading up to the
    output it will return the number of pixels lost/gained on all image edges.
            
    Rules for calculation: for pooling, any lost or added pixels are always at 
    the right side or bottom of the image. In the case of ignore_border=True
    pixels are lost, this can be calculated by upsampling the output_shape
    with the stride and comparing to the input. This also holds for
    ignore_border is False, except here pixels are gained after upsampling.
            
    For convolutional layers the settings valid, full and same are supported.
    In the case of valid convolution, the left and up side always loses 
    filter size - 1 pixels. The right side pixels can be calculated by
    multiplying the output_shape with the stride, adding the pixels for
    left and top, the right and bottom pixels are then the remaining.
    In the case of 'same' convolution, the behavior is the same as 
    pooling with ignore_border=False.
    For full convolution, upsample with the stride and then take the
    input_shape size elements from filter-size - 1 pixels on the left and top.   
    """
    import lasagne
    import lasagne.layers.dnn
    import deeplearning.lasagne_extensions.softmax_layer as dlex_sml

    
    ls = lasagne.layers.get_all_layers(layer)
    for lay in ls:
        if not isinstance(lay, (lasagne.layers.InputLayer,
                              lasagne.layers.Pool2DLayer,
                              lasagne.layers.Conv2DLayer,
                              lasagne.layers.NonlinearityLayer,
                              lasagne.layers.DropoutLayer,
                              lasagne.layers.BatchNormLayer,
                              lasagne.layers.merge.ElemwiseSumLayer,
                              lasagne.layers.dnn.Conv2DDNNLayer,
                              dlex_sml.Softmax4D)):
            print str(lay) + " is a non-supported layer."
            return
    last_shape = np.array(input_image_shape)
    lost = []
    downsamples = []
    for l in ls:
        lost_this_layer = [0,0,0,0] # left, right, top, bottom
        if (not isinstance(l, lasagne.layers.InputLayer) and
            not isinstance(l, lasagne.layers.special.NonlinearityLayer) and
            not isinstance(l, lasagne.layers.noise.DropoutLayer) and
            not isinstance(l, lasagne.layers.normalization.BatchNormLayer) and
            not isinstance(l, lasagne.layers.merge.ElemwiseSumLayer) and
            not isinstance(l, dlex_sml.Softmax4D)):
            cur_stride = l.stride            
            next_shape = np.array(l.get_output_shape_for(last_shape)) 
            up_shape = next_shape[-2:] * np.array(cur_stride)
            if isinstance(l, (lasagne.layers.Conv2DLayer,
                              lasagne.layers.dnn.Conv2DDNNLayer)):
                if l.pad == (0,0) or l.pad == "valid":
                    lost_this_layer[0] = (l.filter_size[0] - 1) // 2 
                    lost_this_layer[2] = (l.filter_size[0] - 1) // 2
                    lost_this_layer[1] = last_shape[-1] - up_shape[1] - lost_this_layer[0]
                    lost_this_layer[3] = last_shape[-2] - up_shape[0] - lost_this_layer[2]
                elif l.pad == "full":
                    lost_this_layer[0] = -((l.filter_size[0] - 1) // 2)
                    lost_this_layer[2] = -((l.filter_size[0] - 1) // 2)
                    lost_this_layer[1] = -(up_shape[1] - last_shape[-1] + lost_this_layer[0])
                    lost_this_layer[3] = -(up_shape[0] - last_shape[-2] + lost_this_layer[2])
                elif l.pad == "same" or l.pad == (((l.filter_size[0] - 1) // 2), ((l.filter_size[1] - 1) // 2)):
                    lost_this_layer[1] = last_shape[-1] - up_shape[1]
                    lost_this_layer[3] = last_shape[-2] - up_shape[0]                    
                else:
                    print "Unsupported padding " + str(l.pad) + " for layer "+ str(l)
                    return
            elif isinstance(l, lasagne.layers.Pool2DLayer):
                if l.ignore_border:                    
                    lost_this_layer[1] = last_shape[-1] - up_shape[1]
                    lost_this_layer[3] = last_shape[-2] - up_shape[0]
                else:
                    lost_this_layer[1] = last_shape[-1] - up_shape[1]
                    lost_this_layer[3] = last_shape[-2] - up_shape[0]                                        
            last_shape = next_shape
            lost.append(np.array(lost_this_layer))
            downsamples.append(np.array(cur_stride))
    if separate:
        return lost, downsamples
    else :
        for i in range(1, len(downsamples)):
            downsamples[i] *= downsamples[i-1]
            lost[i][0:2] *= downsamples[i-1][0]
            lost[i][2:] *= downsamples[i-1][1]
        return np.array(lost).sum(axis=0), downsamples[-1]

        